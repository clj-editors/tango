(ns tango.ui.interactive-test
  (:require [clojure.test]
            [devcards.core :as cards :include-macros true]
            [check.async :refer [async-test check testing]]
            [tango.integration.fake-editor :as fake]
            [promesa.core :as p]))

(cards/defcard-rg fake-editor
  fake/editor
  fake/state)

(cards/deftest interactive-renderer
  (async-test "Interactive Renderer" {:teardown (fake/disconnect!)
                                      :timeout 8000}
    (fake/connect! {:notify prn})

    (testing "simple HTML elements"
      (fake/changing-result [result]
        (fake/type-and-eval "^:tango/interactive {:html [:div \"Hello World\"]}")
        (check result => "Hello World")))

    (testing "counter example - local-only functions to change the state"
      (fake/changing-result [result]
        (fake/type-and-eval (str "^:tango/interactive "
                                 (pr-str {:html ''[:button {:on-click (fn [_]
                                                                        (swap! ?state inc))}
                                                   @?state]
                                          :state 0})))
        (check result => "0"))

      (fake/changing-result [result]
        (p/then (fake/find-element "button" identity 0) #(.click %))
        (check result => "1")))

    (testing "forcing errors"
      (fake/changing-result [result]
        (fake/type-and-eval (str "^:tango/interactive "
                                 (pr-str {:html ''[:div "Hello" ("one" "two")]})))
        (check result => "0"))

      #_
      (fake/changing-result [result]
        (p/then (fake/find-element "button" identity 0) #(.click %))
        (check result => "1")))))
