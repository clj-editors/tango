(ns tango.features.definition-helper
  (:require [tango.features.definition-child :as c :refer [other-var]]))

(defn some-function []
  (str "Just a function " other-var))
