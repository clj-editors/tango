(ns tango.editor-integration.renderer.edn-test
  (:require [clojure.string :as str]
            [tango.integration.fake-editor :as fake]
            [clojure.test]
            [check.async :refer [check async-test testing let-testing]]
            [tango.integration.ui-macros :as m]
            [devcards.core :as cards :include-macros true]
            [tango.editor-integration.renderer.edn :as edn]
            [tango.editor-integration.renderer.protocols :as proto]
            [tango.editor-integration.commands :as cmds]
            [tango.editor-helpers :as helpers]
            [promesa.core :as p]

            ["react-dom" :as rdom]))

(defonce ^:private copied (atom nil))
(defn- simple-render! [obj]
  (let [prom (p/deferred)
        state (atom nil)]
    (reset! state {:run-callback (partial cmds/run-callback! state)
                   :editor/features {:eql (fn [ & args] nil)}
                   :editor/callbacks
                   (merge fake/default-callbacks
                          {:on-copy #(p/resolve! prom %)
                           :notify prn})})
    (reset! copied prom)
    (swap! fake/state assoc :parsed-result (proto/as-html (edn/->EDN obj state) nil nil))))

(cards/deftest renderers-for-simple-edn
  (async-test "Rendering simple EDN (no interaction with the REPL)"
    {:timeout 20000
     :teardown (swap! fake/state dissoc :parsed-result)}

    (testing "rendering leaf elements"
      (fake/changing-result res
        (simple-render! :a-keyword)
        (check res => ":a-keyword"))

      (testing "will copy to clipboard"
        (fake/just-click-nth-link 1)
        (check @copied => ":a-keyword"))

      (fake/changing-result res
        (simple-render! 'a-sym)
        (check res => "a-sym"))

      (fake/changing-result res
        (simple-render! 10)
        (check res => "10"))

      (fake/changing-result res
        (simple-render! "a-string")
        (check res => "\"a-string\""))

      (fake/changing-result res
        (simple-render! nil)
        (check res => "nil"))
      (fake/changing-result res
        (simple-render! true)
        (check res => "true"))
      (fake/changing-result res
        (simple-render! false)
        (check res => "false"))

      (fake/changing-result res
        (simple-render! #"lol.*nada")
        (check res => "#\"lol.*nada\"")))

     ; #_(def obj (helpers/IncompleteString. "some really big str"))
     ; (let-testing "incomplete strings" [obj (helpers/IncompleteString. "some really big str")]
     ;   (aset obj "__more__" (constantly #(str % "ing")))
     ;   (fake/changing-result res
     ;     (simple-render! obj)
     ;     (check (fake/normalize res) => "\"some really big str ... \""))
     ;
     ;   (testing "expands the completion"
     ;     (fake/changing-result res
     ;       (fake/just-click-nth-link 1)
     ;       (check (fake/normalize res) => "\"some really big string\"")))))

    (testing "rendering collections"
      (fake/changing-result res
        #_(simple-render! (->> (range 24000) vec))
        (simple-render! [1 2 3])
        (check (fake/normalize res) => "[ 1 2 3 ]")

        #_(simple-render! '[1 [[2]] 3 ([1] 2) (2 3)])
        #_(rdom/render (proto/as-html (edn/->EDN (vec (range 8000)) {}) nil nil)
                       (js/document.getElementById "lol"))
        #_(simple-render! (->> (range 4000) vec))
        #_(simple-render! (->> (range 8000) vec))
        #_(simple-render! (->> (range 8000)  (map (constantly ["1" 2 3])) vec))
        #_(swap! fake/state dissoc :parsed-result)
        #_(simple-render! '[1 [[2]] 3 ([1] 2) (2 3)])
        #_(simple-render! #{1 2 3 4 5})
        #_(simple-render! '{:a 10 :b [[20]] :c [[1] 2 3]})
        #_(simple-render! (->> (range 8000)
                               (map (fn [e] [e [e [e]]]))
                               (into {}))))

      (testing "will copy"
        (fake/just-click-nth-link 2)
        (check @copied => "[1 2 3]"))

      (let-testing "with lists" [obj (helpers/IncompleteString. "str")]
        (aset obj "__more__" (constantly #(str % "ing")))
        (fake/changing-result res
          (simple-render! (list 1 2 obj))
          (check (fake/normalize res) => "( 1 2 \"str ... \" )"))

        (check (fake/click-nth-link 2)
               => {:normalized "( 1 2 \"string\" )"}))

      (testing "rendering maps"
        (fake/changing-result res
          (simple-render! {:a 1 :b 2})
          (check (fake/normalize res) => "{ :a 1 , :b 2 }"))

        (testing "will copy contents"
          (fake/just-click-nth-link 2)
          (check @copied => "{:a 1, :b 2}"))

        (testing "will open all inner elements by default"
          (check (fake/click-nth-link 1)
                 => {:normalized "{ :a 1 , :b 2 } :a 1 :b 2"})))

      (testing "rendering nested elements"
        (fake/changing-result res
          (simple-render! '[1 (2 3)])
          (check (fake/normalize res) => "[ 1 ( 2 3 ) ]"))

        (testing "copies"
          (fake/just-click-nth-link 2)
          (check @copied => "[1 (2 3)]"))

        (testing "clicking on open/close keeps nested elements open/closed"
          (p/let [original {:normalized "[ 1 ( 2 3 ) ]"}
                  inner-is-closed {:normalized "[ 1 ( 2 3 ) ] 1 ( 2 3 )"}
                  inner-is-open {:normalized "[ 1 ( 2 3 ) ] 1 ( 2 3 ) 2 3"}]
            (check (fake/click-nth-link 1) => inner-is-closed)
            (check (fake/click-nth-link 4) => inner-is-open)
            (check (fake/click-nth-link 1) => original)
            (check (fake/click-nth-link 1) => inner-is-open)))))

    (testing "rendering tagged literals"
      (fake/changing-result res
        (simple-render! (tagged-literal 'some/tagged [1 2 3]))
        (check (fake/normalize res) => "#some/tagged [ 1 2 3 ]"))

      (testing "copies to clipboard"
        (fake/just-click-nth-link 2)
        (check @copied => "#some/tagged [1 2 3]"))

      (testing "clicking on open element opens child"
        (check (fake/click-nth-link 1)
               => {:normalized "#some/tagged [ 1 2 3 ] 1 2 3"})
        (check (fake/click-nth-link 2)
               => {:normalized "#some/tagged [ 1 2 3 ]"}))

      (let-testing "incomplete lists" [obj '(:a :b :c)]
        (aset obj "__more__" (constantly #(concat % '(:d))))
        (fake/changing-result res
          (simple-render! obj)
          (check (fake/normalize res) => "( :a :b :c ... )"))

        (testing "expands the completion"
          (fake/changing-result res
            (fake/just-click-nth-link 2)
            (check (fake/normalize res) => "( :a :b :c :d )")))

        (testing "expands when opened too"
          (fake/changing-result _ (simple-render! obj))
          (fake/changing-result _ (fake/just-click-nth-link 1))
          (fake/changing-result res
            (fake/just-click-nth-link 7)
            (check (fake/normalize res) => "( :a :b :c :d ) :a :b :c :d"))))

      (let-testing "incomplete vectors" [obj [:a :b :c]]
        (aset obj "__more__" (constantly #(conj % :d)))
        (fake/changing-result res
          (simple-render! obj)
          (check (fake/normalize res) => "[ :a :b :c ... ]"))

        (testing "expands the completion"
          (fake/changing-result res
            (fake/just-click-nth-link 2)
            (check (fake/normalize res) => "[ :a :b :c :d ]")))

        (testing "expands when opened too"
          (fake/changing-result _ (simple-render! obj))
          (fake/changing-result _ (fake/just-click-nth-link 1))
          (fake/changing-result res
            (fake/just-click-nth-link 7)
            (check (fake/normalize res) => "[ :a :b :c :d ] :a :b :c :d"))))

      (let-testing "incomplete maps" [obj {:a 1 :b 2}]
        (aset obj "__more__" (constantly #(merge % {:c 3})))
        (fake/changing-result res
          (simple-render! obj)
          (check (fake/normalize res) => "{ :a 1 , :b 2 , ... }"))

        (testing "expands the completion"
          (fake/changing-result res
            (fake/just-click-nth-link 2)
            (check (fake/normalize res) => "{ :a 1 , :b 2 , :c 3 }")))

        (testing "expands when opened too"
          (fake/changing-result _ (simple-render! obj))
          (fake/changing-result _ (fake/just-click-nth-link 1))
          (fake/changing-result res
            (fake/just-click-nth-link 8)
            (check (fake/normalize res) => #"\{ :a 1 , :b 2 , :c 3 \}"))))

      (let-testing "big incomplete maps" [obj {0 1, 32 33, 34 35, 4 5, 36 37, 6 7, 20 21, 22 23, 24 25, 28 29}]
        (aset obj "__more__" (constantly #(merge % {2 3, 38 39, 8 9, 10 11, 12 13, 14 15, 16 17, 18 19, 26 27, 30 31})))
        (reduce (fn [old-prom _]
                  (p/do!
                   old-prom
                   (fake/changing-result res
                     (simple-render! obj)
                     (p/let [res (fake/normalize res)
                             same-beginning (re-pattern (str/replace res #" , \.\.\..*\}" ""))]
                       (fake/changing-result after-expansion
                         (fake/just-click-nth-link 2)
                         (check (fake/normalize after-expansion) => same-beginning))))))
                (p/promise nil)
                (range 10)))

      ; (str/replace "1\n2\n3\n,,,a\nb}" #"(?smi),,,.+\}" "")
      ;
      ; (let-testing "object with more data" [obj 'some-elem]
      ;   (aset obj "__more__" (constantly (helpers/->ObjWithData 'some-elem [1 2 3])))
      ;   (fake/changing-result res
      ;     (simple-render! obj)
      ;     (check (fake/normalize res) => "some-elem ..."))
      ;
      ;   (testing "expands the object"
      ;     (fake/changing-result res
      ;       (fake/just-click-nth-link 1)
      ;       (check (fake/normalize res) => #"some-elem 1 2 3"))))
      ;
      ; (testing "simple error element"
      ;   (fake/changing-result res
      ;     (simple-render! (helpers/Error2. [1 2]
      ;                                      [{:stack-file "something" :row 10 :col 9}]
      ;                                      nil))
      ;     (check (fake/normalize res) => #"\[ 1 2 \]")
      ;     (check (fake/normalize res) => #"something:10:9"))
      ;   (check (fake/click-nth-link 1) => {:normalized #"\[ 1 2 \] 1 2"})
      ,,,)))

(cards/defcard-rg fake-editor
 fake/editor
 fake/state)

#_
(do
  (simple-render! (map range (range 100)))
  nil)
