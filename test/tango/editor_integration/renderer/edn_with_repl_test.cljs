(ns tango.editor-integration.renderer.edn-with-repl-test
  (:require [clojure.string :as str]
            [tango.integration.fake-editor :as fake]
            [clojure.test]
            [check.async :refer [check async-test testing let-testing]]
            [tango.integration.ui-macros :as m]
            [devcards.core :as cards :include-macros true]
            [tango.editor-integration.renderer.edn :as edn]
            [tango.editor-integration.renderer.protocols :as proto]
            [tango.editor-integration.commands :as cmds]
            [tango.editor-helpers :as helpers]
            [promesa.core :as p]))

(defonce ^:private copied (atom nil))
(defonce ^:private open-editor (atom nil))
(defn- render! [obj]
  (let [prom (p/deferred)
        open-prom (p/deferred)]
    (reset! open-editor open-prom)
    (swap! fake/state assoc
           :parsed-result (proto/as-html (edn/->EDN obj (:editor-state @fake/state)) nil nil))))

#_
(let [eql (-> @fake/state
              :editor-state
              deref
              :editor/features
              :eql)]
  (eql [:config/eval-as]))

(cards/deftest exception-renderers
  (async-test "Rendering exceptions, and clicking on stacktraces"
    {:timeout 20000
     :teardown (fake/disconnect!)}

    (let [open-prom (p/deferred)]
      (fake/connect! {:open-editor #(p/resolve! @open-editor %)}))

    (testing "java exception"
      (fake/changing-result res
        (render! (helpers/Error2. [1 2]
                                  '[{:class clojure.lang.AFn
                                     :method call
                                     :stack-file "AFn.java"
                                     :row 18}
                                    {:class clojure.core$apply
                                     :method invokeStatic
                                     :stack-file "core.clj"
                                     :row 200}]
                                  nil))
        (check (fake/normalize res) => #"in clojure\.lang\.AFn\.call .*AFn\.java")
        (check (fake/normalize res) => #"in clojure.core/apply .*core\.clj:200")))

    (testing "will go to definition"
      (fake/just-click-nth-link 2)
      (check @open-editor => {:file-name #"core\.clj" :line 199}))

    (testing "JS exception"
      ; (fake/changing-result res)
      (render! (helpers/Error2. [1 2]
                                [{:class "new cljs$core$ExceptionInfo"
                                  :stack-file "target/integration/cljs-runtime/cljs.core.js"
                                  :row 37684
                                  :col 10}]
                                nil))
      (fake/wait-for-p #(p/let [res (fake/normalize (fake/txt-for-selector "#result"))]
                          (re-find #"core\.cljs" res)))
      (check (fake/normalize (fake/txt-for-selector "#result"))
             => #"in new cljs.core/ExceptionInfo.*cljs/core.cljs:11472:10"))

    (testing "will go to definition"
      (fake/just-click-nth-link 2)
      (check @open-editor => {:file-name #"jar!/cljs/core.cljs" :line 11471 :column 9}))))

(cards/defcard-rg fake-editor
 fake/editor
 fake/state)
