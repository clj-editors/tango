(ns tango.integration.fake-editor)

(defmacro changing-result [binding & body]
  `(let [~(first binding) (change-result-p)
         res# (promesa.core/do! ~@body)]
     (promesa.core/do!
      ~(first binding)
      res#)))

(defmacro changing-stdout [binding & body]
  `(let [~(first binding) (change-stdout-p)]
     (promesa.core/do! ~@body)
     ~(first binding)))
