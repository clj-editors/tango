(ns tango.integration.fake-editor
  (:require-macros [tango.integration.fake-editor :as f])
  (:refer-clojure :exclude [type])
  (:require [clojure.string :as str]
            [promesa.core :as p]
            [reagent.core :as r]
            [tango.editor-integration.renderer :as render]
            [tango.integration.connection :as conn]
            [orbit.evaluation :as eval]))

(defn wait-for-p [f]
  (p/loop [t 0]
    (when (< t 100)
      (p/let [res (f)]
        (if res
          res
          (p/do!
           (p/delay 100)
           (p/recur (inc t))))))))

(defonce state (r/atom {:host "localhost"
                        :filename "foo.clj"
                        :port 3322
                        :code "(do (defrecord Foo [a b]) (->Foo (range 20) 20))"
                        :repl nil
                        :commands {}
                        :stdout nil
                        :stderr nil
                        :range [[0 0] [0 0]]
                        :eval-result (r/atom nil)}))

(defn- reset-state! []
  (swap! state merge {:code ""
                      :filename "file.clj"
                      :stdout nil
                      :stderr nil
                      :parsed-result nil
                      :range [[0 0] [0 0]]
                      :eval-result (r/atom nil)}))

(defn- gen-result [result]
  (p/let [parse (-> @state :features :result-for-renderer)
          res (parse result)]
    (reset! (:eval-result @state) res)))

(defn run-command! [command]
  (if-let [cmd (get-in @state [:commands command :command])]
    (cmd)
    (prn "Command not found" command)))

(defn evaluate []
  (let [lines (-> @state :code str/split-lines)]
    (swap! state assoc :range [[0 0] [(-> lines count dec) (-> lines last count dec)]])
    (run-command! :evaluate-selection)))

(defn type [txt] (swap! state assoc :code txt))
(defn type-and-eval [txt]
  (type txt)
  (evaluate))

(defn txt-for-selector [sel]
  (str (some-> js/document
               (.querySelector sel)
               .-innerText
               .trim)))

(defn change-result-p []
  (let [old (txt-for-selector "#result")]
    (wait-for-p #(and (not= old (txt-for-selector "#result"))
                      (txt-for-selector "#result")))))

(defn change-stdout-p []
  (let [old (txt-for-selector "#stdout")]
    (wait-for-p #(and (not= old (txt-for-selector "#stdout"))
                      (txt-for-selector "#stdout")))))

(defn change-stderr-p []
  (let [old (txt-for-selector "#stderr")]
    (wait-for-p #(and (not= old (txt-for-selector "#stderr"))
                      (txt-for-selector "#stderr")))))

(defn handle-disconnect []
  (reset! (:eval-result @state) nil)
  (swap! state assoc
         :repl nil
         :stdout nil :stderr nil
         :commands {}))

(def default-callbacks
  {:on-disconnect handle-disconnect
   :on-stdout #(swap! state update :stdout (fn [e] (str e %)))
   :on-start-eval #(reset! (:eval-result @state) nil);#(prn :FOO %)
   :on-eval gen-result
   :notify identity
   :prompt (constantly (. js/Promise resolve "fixture"))
   :get-config (constantly {:eval-mode :prefer-clj
                            :project-paths [(. js/process cwd)]})
   :on-stderr #(swap! state update :stderr (fn [e] (str e %)))
   :editor-data #(let [code (:code @state)]
                   {:contents code
                    :filename (:filename @state)
                    :range (:range @state)})})

(defn connect!
  ([] (connect! {}))
  ([additional-callbacks]
   (reset-state!)
   (if (:repl @state)
     (.resolve js/Promise @state)
     (.
       (conn/connect! (:host @state) (:port @state)
                      (merge default-callbacks additional-callbacks))
       (then (fn [res]
               (swap! state assoc
                      :editor-state res
                      :repl (:repl/evaluator @res)
                      :commands (:editor/commands @res)
                      :features (:editor/features @res)
                      :stdout "" :stderr "")))))))

(defn disconnect! [] (-> @state :repl eval/close!))

; (connect!)
; (eval/evaluate (:repl @state) "(+ 1 2)")
; (eval/close! (:repl @state))

(defn editor [state]
  [:div
   [:h4 "Socket REPL connections"]
   [:p [:b "Hostname: "] [:input {:type "text" :value (:host @state)
                                  :on-change #(->> % .-target .-value (swap! state assoc :host))}]
    [:b " Port: "] [:input {:type "text" :value (:port @state)
                            :on-change #(->> % .-target .-value int (swap! state assoc :port))}]
    [:b " Filename: "] [:input {:type "text" :value (:filename @state)
                                :on-change #(->> % .-target .-value (swap! state assoc :filename))}]]
   [:textarea {:style {:width "100%" :height "100px"}
               :value (:code @state)
               :on-change #(->> % .-target .-value (swap! state assoc :code))}]
   [:div
    (when (:repl @state)
      (for [[command] (:commands @state)]
        [:button {:key command
                  :on-click #(run-command! command)}
         (pr-str command)]))]
   [:div
    (if (:repl @state)
      [:span
       [:button {:on-click evaluate}
        "Evaluate"] " "
       [:button {:on-click disconnect!} "Disconnect!"]]
      [:button {:on-click #(connect!)} "Connect!"])
    [:p (if (:repl @state) "Connected" "Disconnected")]]
   [:div
    [:div
     [:h5 "RESULT"]
     [:pre
      [:div {:id "result" :class "result tango"}
       (when-let [res @(:eval-result @state)]
         [render/view-for-result res])
       (when-let [res (:parsed-result @state)]
         res)]]]]
   [:div
    [:h5 "STDOUT"
     (when-let [out (:stdout @state)]
       [:pre#stdout out])]]
   [:div
    [:h5 "STDERR"]]
   (when-let [out (:stderr @state)]
     [:pre#stderr out])])

(defn find-element [selector filter-fn index]
  (p/let [elem-fn (fn []
                    (try
                      (nth
                       (->> (str "div.result " selector)
                            (.querySelectorAll js/document)
                            (filter filter-fn))
                       index)
                      (catch :default _)))]
    (wait-for-p elem-fn)))

(defn click-link [link-text]
  (p/let [link (find-element "a"
                             #(->> % .-innerText (re-find (re-pattern link-text)))
                             0)]
    (.click link)))

(defn click-more-link [zero-based-index]
  (p/let [link (find-element "a.more-info" identity zero-based-index)]
    (.click link)))

(defn click-open-close [zero-based-index]
  (p/let [link (find-element "a.chevron" identity zero-based-index)]
    (.click link)))

(defn normalize [res]
  (p/let [res res]
    (str/replace (str res) #"(\n|\s+)+" " ")))

(defn clear-results! []
  (reset! (:eval-result @state) nil)
  (swap! state assoc :stderr "" :stdout ""))

(defn- click-selector [sel]
  (let [p (p/deferred)]
    (f/changing-result [res]
      (-> js/document (.querySelector sel) .click)
      (p/let [original res
              normalized (normalize res)]
        (p/resolve! p {:normalized normalized :original original})))
    p))

(defn click-nth-link [number]
  (let [p (p/deferred)]
    (f/changing-result [res]
      (p/let [elements (into [] (js/document.querySelectorAll "#result a"))
              element (nth elements (dec number))
              _ (.click element)
              original res
              normalized (normalize res)]
        (p/resolve! p {:normalized normalized :original original})))
    p))

(defn just-click-nth-link [number]
  (let [elements (into [] (js/document.querySelectorAll "#result a"))
        element (nth elements (dec number))]
    (.click element)))
