(ns tango.integration.ui
  (:require [devcards.core :as cards :include-macros true]
            [tango.integration.connection-test]
            [tango.ui.interactive-test]))
            ; [flow-storm.api]))
  ;           [schema.core :as s]
  ;
  ;           [tango.editor-helpers-test]
  ;           [tango.repl-client.parsing-test]
  ;           [tango.integration.clojurescript-ui]
  ;           [tango.integration.clojure-ui]
  ;           [tango.repl-client.evaluation-test]
  ;           [tango.features.autocomplete-test]
  ;           [tango.features.shadow-cljs-test]
  ;           [tango.editor-integration.autocomplete-test]
  ;           [tango.repl-client.connection-test]
  ;           [tango.integration.rendered-actions]
  ;           [tango.editor-integration.renderer.interactive-test]
  ;           [tango.editor-integration.doc-test]
  ;           [tango.nrepl.nrepl-test]
  ;           [tango.editor-integration.configs-test]
  ;           [tango.editor-integration.pathom-test]
  ;           [tango.editor-integration.stacktraces-test]
  ;           [tango.editor-integration.renderer.edn-test]
  ;           [tango.editor-integration.renderer.edn-with-repl-test]))

(cards/start-devcard-ui!)
; (s/set-fn-validation! true)
