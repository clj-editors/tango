(ns tango.integration.connection-test
  (:require [devcards.core :as cards :include-macros true]
            [clojure.test]
            [check.async :refer [async-test check testing]]
            [tango.integration.fake-editor :as fake]
            [promesa.core :as p]))

(cards/defcard-rg fake-editor
  fake/editor
  fake/state)

(set! cards/test-timeout 20000)

(cards/deftest repl-evaluation
  (async-test "Clojure REPL evaluation" {:teardown (fake/disconnect!)
                                         :timeout 8000}
    (fake/connect! {:notify prn})

    (testing "evaluation works"
      (fake/changing-result [result]
        (fake/type-and-eval "(+ 2 3)")
        (check result => "5")))

    (testing "rendering vectors"
      (p/let [fully-collapsed "[ 1 [ 2 ] ]"
              opened-once (str fully-collapsed " 1 [ 2 ]")
              fully-open (str opened-once " 2")]
        (fake/changing-result [result]
          (fake/type-and-eval "[1 [2]]")
          (check (fake/normalize result) => fully-collapsed))

        (fake/changing-result [result]
          (fake/click-open-close 0)
          (check (fake/normalize result) => opened-once))

        (testing "deep vectors should keep their open/close state"
          (fake/changing-result [result]
            (fake/click-open-close 1)
            (check (fake/normalize result) => fully-open))

          (fake/changing-result [result]
            (fake/click-open-close 0)
            (check (fake/normalize result) => fully-collapsed))
          (fake/changing-result [result]
            (fake/click-open-close 0)
            (check (fake/normalize result) => fully-open)))))

    (testing "rendering lists or sets"
      (fake/changing-result [result]
        (fake/type-and-eval "'(1 2)")
        (check (fake/normalize result) => "( 1 2 )"))
      (fake/changing-result [result]
        (fake/click-open-close 0)
        (check (fake/normalize result) => "( 1 2 ) 1 2"))

      (fake/changing-result [result]
        (fake/type-and-eval "#{1 2}")
        (check (fake/normalize result) => "#{ 1 2 }")))

    (testing "rendering maps"
      (fake/changing-result [result]
        (fake/type-and-eval "{:a 10, :b 20}")
        ;; FIXME - this
        (check (fake/normalize result) => "{ :a 10 , :b 20 }"))
      (fake/changing-result [result]
        (fake/click-open-close 0)
        (check (fake/normalize result) => "{ :a 10 , :b 20 } :a 10 :b 20")))

    (testing "rendering tagged literals"
      (fake/changing-result [result]
        (fake/type-and-eval "(tagged-literal 'foo [1 2 3])")
        (check (fake/normalize result) => "#foo [ 1 2 3 ]"))

      (testing "clicking on open expands children too"
        (fake/changing-result [result]
          (fake/click-open-close 0)
          (check (fake/normalize result) => "#foo [ 1 2 3 ] 1 2 3"))))

    (testing "rendering complicated tagged literals"
      (fake/changing-result [_]
        (fake/type-and-eval "(tagged-literal 'parent (tagged-literal 'child [1]))"))
      (fake/changing-result [result]
        (fake/click-open-close 0)
        (check (fake/normalize result) => "#parent #child [ 1 ] 1")))

    (testing "renders Java objects"
      (fake/changing-result [result]
        (fake/type-and-eval "Object")
        (check result => #"java\.lang\.Object"))

      (fake/changing-result [result]
        (fake/click-more-link 0)
        (check result => #"Constructors")
        (check (fake/normalize result) => #"( Object. )")
        (check result => #"Methods"))

      (testing "opening Java classes inside the reflection info"
        (fake/changing-result [_] (fake/click-open-close 1))
        (fake/changing-result [result]
          (fake/click-more-link 0)
          (check (fake/normalize result) => #"Methods.*Methods"))))))
