(ns tango.integration.repl-helpers
  (:require [promesa.core :as p]))

(defn prepare-new-eql [state]
  (let [eql (-> @state :editor/features :eql)
        cached-result (eql [:editor/data :config/repl-kind
                            :config/eval-as :config/project-paths
                            :repl/evaluator])]
    (fn q
      ([query] (q {} query))
      ([seed query]
       (p/let [original-seed cached-result]
         (eql (merge original-seed seed) query))))))
