(ns tango.integration.interpreter
  (:require [sci.core :as sci]
            ["fs" :as fs]
            ["path" :as path]
            [clojure.string :as str]
            [clojure.zip]
            [tango.integration.interpreter.pprint :as cljs-pprint-config]
            [tango.integration.interpreter.promesa :as promesa-config]
            [tango.integration.interpreter.reagent :as reagent-config]
            [tango.state :as state]
            [promesa.core :as p]
            [sci.async :as scia]
            [rewrite-clj.node]
            [rewrite-clj.parser]
            [rewrite-clj.zip]
            [orbit.evaluation :as eval]))

(sci/enable-unrestricted-access!) ;; allows mutating and set!-ing all vars from inside SCI

(defn- ns->path [config-path namespace]
  (str (apply path/join
         config-path
         (-> (str namespace)
             (munge)
             (str/split #"\.")))
       ".cljs"))

(defn load-fn [config-dir {:keys [libname opts _ns _ctx] :as c}]
  (if (string? libname)
    (let [req (gensym "required")
          req `(let [~req (js/require ~libname)]
                 ~(when-let [as (:as opts)]
                    `(def ~as ~req))
                 ~@(for [refer (:refers opts)]
                     `(def ~refer (. ~req ~(symbol (str "-" refer))))))]
      {:source (pr-str req)})
    (let [ns-file-path (ns->path (path/dirname config-dir) libname)
          contents (and (fs/existsSync ns-file-path)
                        (fs/readFileSync ns-file-path #js {:encoding "UTF-8"}))]
      (if contents
        {:source contents}
        (throw (ex-info (str "Can't load namespace " libname) {}))))))

(def zns (sci/create-ns 'clojure.zip nil))
(def zip-namespace
  (sci/copy-ns clojure.zip zns))

(def rzns (sci/create-ns 'rewrite-clj.zip))
(def rewrite-clj-zip-ns (sci/copy-ns rewrite-clj.zip rzns))

(def rpns (sci/create-ns 'rewrite-clj.parser))
(def rewrite-clj-parser-ns (sci/copy-ns rewrite-clj.parser rpns))

(def rnns (sci/create-ns 'rewrite-clj.node))
(def rewrite-clj-node-ns (sci/copy-ns rewrite-clj.node rnns))

(def namespaces
  {'clojure.zip zip-namespace
   'cljs.pprint cljs-pprint-config/cljs-pprint-namespace
   'promesa.core promesa-config/promesa-namespace
   'rewrite-clj.zip rewrite-clj-zip-ns
   'rewrite-clj.parser rewrite-clj-parser-ns
   'reagent.core reagent-config/reagent-namespace
   'rewrite-clj.node rewrite-clj-node-ns})
   ; 'render (render-ns editor-state)
   ; 'editor (editor-ns repl editor-state)})

(defn- make-temp-let-bindings [ctx bindings]
  (let [ns-name (gensym "-_-temp-ns-_-")]
    (swap! (:env ctx) assoc-in [:namespaces ns-name] bindings)
    {:let-bindings (->> bindings
                        keys
                        (reduce (fn [acc var-name] (str acc " " var-name " " ns-name "/" var-name))
                                ""))
     :ns-name ns-name}))

(defn- norm-possible-promise [id [result]]
  {:id id :result result})

(defrecord Evaluator [default-ns ctx on-patch]
  eval/REPL
  (-evaluate [_ code options]
    (let [id (:id options (str (gensym "eval-")))
          namespace (:namespace options default-ns)
          _ (sci/eval-form ctx `(~'in-ns '~namespace))
          {:keys [let-bindings ns-name]} (when-let [b (-> options :options :bindings)]
                                           (make-temp-let-bindings ctx b))
          normalized-code (cond
                            let-bindings (str "(let [" let-bindings "] [" code "\n])")
                            (re-find #"\(\s*ns" code) (str code "[]")
                            :else (str "[" code "\n]"))
          result (scia/eval-string* ctx normalized-code)]
      (-> result
          (p/then #(norm-possible-promise id %))
          (p/catch #(hash-map :id id :error %))
          (p/finally #(when ns-name (swap! (:env ctx) update :namespaces dissoc ns-name))))))

  (-break [_ _])
  (-close [_])
  (-is-closed [_] false))

(def ^:private requires
  '[[clojure.string :as str]
    [clojure.walk :as walk]
    [promesa.core :as p]
    [reagent.core :as r]
    [tango.eql :as eql :refer [eql]]
    [callback :as callback]
    [ui :as ui]
    [editor :as editor]])

(defn default-code [default-ns]
  (str "(ns " default-ns "
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [promesa.core :as p]
            [reagent.core :as r]
            [tango.eql :as eql :refer [eql]]
            #_
            [tango.editor.callback :as callback]))"))

#_
(defn- editor-ns [repl state]
  (let [repl (delay (or repl (find-repl state)))]
    {'run-callback (partial cmds/run-callback! state)
     'run-feature (fn [cmd & args]
                    (p/let [curr-repl @repl]
                      (if (= cmd :go-to-var-definition)
                        (cmds/run-feature! state
                                           :go-to-var-definition
                                           (assoc (first args)
                                                  :repl curr-repl))
                        (apply cmds/run-feature! state cmd args))))
     'add-resolver pathom/add-resolver
     'compose-resolver pathom/compose-resolver}))

(defn- make-current-doc [thing fun]
  (with-meta
    fun
    {:doc
     (str/replace
      "Gets the current THING from the editor.

For more info on what that means, see: [https://gitlab.com/clj-editors/tango/-/blob/master/doc/FAQ.md?ref_type=heads#what-is-a-block-top-block](https://gitlab.com/clj-editors/tango/-/blob/master/doc/FAQ.md?ref_type=heads#what-is-a-block-top-block)
Returns a promise with :text/contents and :text/range, containing the contents and the 'range' of
THING. The 'range' is zero-based, meaning that row number 0 is the first one, and it's in the
format of `[start end]`, where start is `[row col]`."
      "THING" thing)}))

(defn- make-eval-doc [thing fun]
 (with-meta
   fun
   {:doc
    (str/replace
     "Gets the current THING from the editor.

For more info on what that means, see: [https://gitlab.com/clj-editors/tango/-/blob/master/doc/FAQ.md?ref_type=heads#what-is-a-block-top-block](https://gitlab.com/clj-editors/tango/-/blob/master/doc/FAQ.md?ref_type=heads#what-is-a-block-top-block)
Returns a promise with :text/contents and :text/range, containing the contents and the 'range' of
THING. The 'range' is zero-based, meaning that row number 0 is the first one, and it's in the
format of `[start end]`, where start is `[row col]`."
     "THING" thing)}))

(defn- make-editor-ns [state commands]
  (let [eql (-> @state :editor/features :eql)
        editor-data (-> @state :editor/callbacks :editor-data)]
    {'register-command ^{:doc "Register a new command on the editor.

- `name` is a `kebab-case` name for the command
- `command` is the command itself (a function)"} (fn [name command]
                                                   (swap! commands conj [name {:command command}])
                                                   nil)

     'get-config (-> @state :editor/callbacks :get-config)
     'data editor-data
     'to-hiccup (fn [result]
                  (let [state (state/get-state (:language (editor-data)))
                        to-hiccup (-> @state :editor/features :original-result-for-renderer)]
                    (to-hiccup {:result result} state)))
     'register-renderer (fn [fun]
                          (swap! state assoc-in
                                 [:editor/features :result-for-renderer]
                                 #(fun (:result %))))
     'evaluate-top-block (-> @state :editor/commands :evaluate-top-block :command)
     'evaluate-block (-> @state :editor/commands :evaluate-block :command)
     'evaluate-selection (-> @state :editor/commands :evaluate-selection :command)
     'break-evaluation (with-meta (-> @state :editor/commands :break-evaluation :command)
                         {:doc "Breaks current code that's being evaluated"})

     'current-top-block (make-current-doc "top block" #(p/then (eql [:text/top-block]) :text/top-block))
     'current-block (make-current-doc "block" #(p/then (eql [:text/block]) :text/block))
     'current-var (make-current-doc "current var" #(p/then (eql [:text/current-var]) :text/current-var))
     'current-selection (make-current-doc "editor's selection" #(p/then (eql [:text/selection]) :text/selection))
     'current-namespace (make-current-doc "current namespace" #(p/then (eql [:text/ns]) :text/ns))}))

(defn- make-repl-ns [state]
  (let [eql (-> @state :editor/features :eql)
        features (:editor/features @state)
        {:keys [result-for-renderer start-eval did-eval]} features]
    {'evaluate (fn e
                 ([contents] (e contents {}))
                 ([{:keys [text/contents text/range]} options]
                  (eql {:text/contents contents :text/range range}
                       [(list :repl/result options)])))
     'result-for result-for-renderer
     'start-eval start-eval
     'did-eval did-eval}))

(defn- make-eql-ns [state]
  (let [eql ^{:doc "Makes an EQL query against the editor and the REPL. Documentation
can be found at [https://gitlab.com/clj-editors/duck-repled](https://gitlab.com/clj-editors/duck-repled)"}
        (fn eql
          ([q] (eql {} q))
          ([entity q] ((-> @state :editor/features :eql) entity q)))]
    {'eql eql
     'query eql
     'add-resolver (-> @state :editor/features :pathom/add-resolver)
     'compose-resolver (-> @state :editor/features :pathom/compose-resolver)}))

(def ^:private ui-ns
  (sci/copy-ns tango.ui.elements (sci/create-ns 'ui nil)))

(defn- bindings [config-dir {:keys [on-stdout]}]
  {'require scia/require
   'js-require (fn [arg]
                 (let [dir (path/join config-dir "node_modules")
                       _ (.. js/module -paths (unshift dir))
                       res (js/require arg)]
                   (.. js/module -paths shift)
                   res))
   'create-element (fn [e] (.createElement js/document e))
   'dirname config-dir
   'println (fn [& args]
              (on-stdout (str (str/join " " args) "\n"))
              nil)
   'print (fn [& args]
            (on-stdout (str/join " " args))
            nil)
   'prn (fn [& args]
          (->> args
               (map pr-str)
               (str/join " ")
               (#(str % "\n"))
               on-stdout)
          nil)
   'log (fn [& args] (apply js/console.log args))
   'pr (fn [& args]
         (->> args (map pr-str)
              (str/join " ")
              on-stdout)
         nil)})

(defn- make-with-project-macro! [evaluator]
  (eval/evaluate evaluator "(defmacro with-project [project-path & body]
    `(let [paths# (if (string? ~project-path)
                    [(re-pattern ~project-path)]
                    (map re-pattern ~project-path))
           match?# (->> (get-config)
                        :project-paths
                        (some (fn [path#] (some #(re-find % path#) paths#))))]
        (when match?# ~@body)))"
    {:namespace 'editor}))

(defn- seed-to-interpreter [evaluator config-file default-ns]
  (eval/evaluate evaluator (default-code default-ns))
  (doseq [req requires] (eval/evaluate evaluator (str "(require '" (pr-str req) ")")))

  (eval/evaluate evaluator "(defmacro defcommand [name & body]
    `(do (defn- ~name [] ~@body) (register-command (str '~name) #(~name))))"
                 {:namespace 'editor})
  (make-with-project-macro! evaluator)
  (when (fs/existsSync config-file)
    (eval/evaluate evaluator (fs/readFileSync config-file #js {:encoding "UTF-8"}))))

(defn generate-evaluator [feature state]
  (let [config-dir (:config/directory @state)
        root-dir-name (demunge (path/relative (path/dirname config-dir)
                                              config-dir))
        config-file (path/join config-dir "config.cljs")
        default-ns (symbol (str root-dir-name ".config"))
        commands (atom [])
        ctx (sci/init {:namespaces (assoc namespaces
                                          'clojure.core (bindings config-dir (:editor/callbacks @state))
                                          'editor (make-editor-ns state commands)
                                          'repl (make-repl-ns state)
                                          'ui ui-ns
                                          'callback {}
                                          'tango.eql (make-eql-ns state))
                       :features (conj #{:tango :cljs} feature)
                       :classes {'js goog/global :allow :all}
                       :async-load-fn #(load-fn config-dir %)})
        evaluator (->Evaluator default-ns ctx (-> @state :editor/callbacks :on-patch))]

    (swap! state assoc-in [:editor/features :result-for-renderer]
           (-> @state :editor/features :original-result-for-renderer))
    [evaluator
     (-> evaluator
         (seed-to-interpreter config-file default-ns)
         (p/then (fn [] @commands)))]))
