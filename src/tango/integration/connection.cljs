(ns tango.integration.connection
  (:require [promesa.core :as p]
            [orbit.nrepl.evaluator :as orbit-nrepl]
            [orbit.shadow.evaluator :as orbit-shadow]
            [tango.commands-to-repl.pathom :as pathom]
            [clojure.string :as str]
            ["fs" :as fs]
            ["path" :as path]
            [orbit.evaluation :as eval]
            [tango.ui.edn :as edn-ui]
            [tango.ui.elements :as ui]
            [tango.integration.interpreter :as int]
            [tango.ui.console :as tango-console]
            [tango.state :as state]
            ;; FIXME: move to Tango
            [saphire.ruby-parsing :as rp]
            [saphire.ui.ruby :as ui-ruby]
            [saphire.code-treatment :as treat]))

(defn- callback-fn [state ^js watcher output]
  (let [{:keys [on-out on-stdout on-stderr on-result on-disconnect on-patch on-diagnostic]}
        (:editor/callbacks @state)]
    (when (nil? output)
      (swap! state assoc-in [:editor/callbacks :on-disconnect] identity)
      (some-> (:repl/evaluator @state) eval/close!)
      (some-> watcher .close)
      (on-disconnect state))

    (doseq [[k v] output] (on-out state k v))
    (when-let [out (:out output)] (on-stdout out))
    (when-let [out (:err output)] (on-stderr out))
    (when (or (contains? output :result)
              (contains? output :error))
      (on-result output))
    (when-not (or (:out output)
                  (:err output)
                  (nil? output)
                  (:patch output))
      (on-diagnostic state output))
    (when on-patch
      (when-let [patch (:orbit.patch/result output)]
        (on-patch (assoc output :result patch)))
      (when-let [patch (:orbit.patch/error output)]
        (on-patch (assoc output :result ^:tango/wrapped-error [patch]))))))

(defn- prepare-patch! [state]
  (let [{:keys [on-patch get-rendered-results]} (:editor/callbacks @state)]
    (when-not on-patch
      (swap! state assoc-in [:editor/callbacks :on-patch]
             (fn [patch]
               (let [parse (-> @state :editor/features :result-for-renderer)]
                 (doseq [res (get-rendered-results)
                         :let [patch-id (:orbit.patch/id patch)]
                         :when (.-querySelectorAll res)
                         :let [new-result (parse patch state)
                               dom (ui/dom new-result)]
                         elem (.querySelectorAll res (str "." patch-id))]
                   (. elem replaceWith dom))))))))

(defn- prepare-console [{:keys [get-console on-stdout on-stderr] :as callbacks}]
  (cond-> callbacks
    (nil? on-stdout) (assoc :on-stdout #(tango-console/stdout (get-console) %))
    (nil? on-stderr) (assoc :on-stderr #(tango-console/stderr (get-console) %))))

(defn- file-exists? [file]
  (js/Promise. (fn [resolve] (fs/exists file resolve))))

(def ^:private default-opts
  {:on-start-eval identity
   :file-exists file-exists?
   :config-file-path nil
   :register-commands identity
   :open-editor identity
   :get-rendered-results (constantly [])
   :on-copy identity
   :on-eval identity
   :on-result identity
   :on-out (constantly nil)
   :on-disconnect identity
   :editor-data identity
   :notify identity
   :get-config (constantly {:project-paths [], :eval-mode :prefer-clj})
   :prompt (fn [ & _] (js/Promise. (fn [])))})

(defn- connection-error! [error notify]
  (if (#{"ECONNREFUSED" -111} error)
    (notify {:type :error
             :title "REPL not connected"
             :message (str "Connection refused. Ensure that you have a "
                           "Socket REPL started on this host/port")})
    (do
      (notify {:type :error
               :title "REPL not connected"
               :message (str "Unknown error while connecting to the REPL: "
                             error)})
      (.error js/console error)))
  nil)

(defn- did-eval [state on-eval {:keys [repl/result repl/error] :as full-result}]
  (let [to-merge (dissoc full-result :com.wsscode.pathom3.connect.runner/attribute-errors)
        final-result (if result
                       (merge result to-merge)
                       ^:tango/error (merge error to-merge))]
    (on-eval state final-result)
    final-result))


(defn- find-definition [state]
  (p/let [eql (-> @state :editor/features :eql)
          details (eql [{:editor/contents
                         [:definition/filename
                           :text/language
                          :definition/contents
                          :definition/row
                          :definition/col]}])]
    (prn :DETAILS details)
    (:editor/contents details)))

(defn- goto-definition [state]
  (p/let [{:definition/keys [filename contents row col]} (find-definition state)
          open-editor (-> @state :editor/callbacks :open-editor)]
    (when (and filename row)
      (open-editor (cond-> {:file-name filename :line row}
                     contents (assoc :contents (:text/contents contents))
                     col (assoc :column col))))))

(defn- doc-for-var [state]
  (p/let [on-start-eval (-> @state :editor/callbacks :on-start-eval)
          eql (-> @state :editor/features :eql)
          id (gensym "doc-for-var-")
          on-start-params (eql [{:editor/contents [:file/filename :text/range :editor/data]}])
          on-start-params (-> on-start-params :editor/contents (assoc :id id))
          _ (on-start-eval state on-start-params)
          res (eql [{:editor/contents [:render/doc]}])
          on-eval (-> @state :editor/callbacks :on-eval)]
    (if-let [interactive (-> res :editor/contents :render/doc)]
      (on-eval state (assoc on-start-params
                            :result (with-meta interactive {:tango/interactive true})))
      (on-eval state (assoc on-start-params
                            :result ^:tango/wrapped-error [(symbol "Couldn't find doc for var")])))))

(defn- eval-text-in-editor-context [state on-start-eval on-eval code params]
  (p/let [eql (-> @state :editor/features :eql delay)
          id (str (gensym "eval-"))
          editor-info (@eql [{:editor/contents [:file/filename
                                                :text/range
                                                :editor/data
                                                :repl/namespace]}])
          editor-info (-> editor-info
                          :editor/contents
                          (dissoc :com.wsscode.pathom3.connect.runner/attribute-errors)
                          (assoc :id id))
          _ (on-start-eval state editor-info)
          eval-params (assoc params :id id)
          query (conj [:text/range :file/filename :editor/data]
                      (list :repl/result eval-params)
                      (list :repl/error eval-params))
          seed (cond-> (assoc editor-info :text/contents code)
                 (:namespace params) (assoc :repl/namespace (:namespace params))
                 (:row params) (assoc-in [:text/range 0 0] (:row params))
                 (:col params) (assoc-in [:text/range 0 1] (:col params)))
          res (@eql seed query)]
    (did-eval state on-eval res)))

(defn- eval-thing [state key]
  (let [eql (-> @state :editor/features :eql delay)
        query [:text/range :file/filename :editor/data]
        on-start-eval (-> @state :editor/callbacks :on-start-eval)
        on-eval (-> @state :editor/callbacks :on-eval)]
    (fn []
      (p/let [id (str (gensym "eval-"))
              editor-info (@eql [{:editor/contents
                                  [{key
                                    [:file/filename :text/range :editor/data :repl/watch-point]}]}])
              _ (on-start-eval state (-> editor-info
                                         :editor/contents
                                         (get key)
                                         (dissoc :com.wsscode.pathom3.connect.runner/attribute-errors)
                                         (assoc :id id)))
              watch-id (get-in editor-info [:editor/contents key :repl/watch-point])
              eval-params (cond-> {:id id}
                            watch-id (assoc :eval-opts {:watch_id ""}))
              query (conj query
                          (list :repl/result eval-params)
                          (list :repl/error eval-params))
              res (@eql [{:editor/contents [{key query}]}])]
        (->> res :editor/contents key (did-eval state on-eval))))))

(defn- commands-for [state]
  (let [eql (-> @state :editor/features :eql delay)
        get-console (-> @state :editor/callbacks :get-console)
        evaluate (-> @state :editor/features :evaluate delay)]
    {:evaluate-top-block {:command (eval-thing state :text/top-block)}
     :evaluate-block {:command (eval-thing state :text/block)}
     :evaluate-selection {:command (eval-thing state :text/selection)}
     :break-evaluation {:command #(-> @state :repl/evaluator eval/break!)}
     :run-tests-in-ns {:command #(do
                                   (@evaluate "(require 'clojure.test)")
                                   (@evaluate "(clojure.test/run-tests)"))}
     :run-test-for-var {:command #(p/let [{:keys [text/current-var]} (@eql [:text/current-var])]
                                    (@evaluate "(require 'clojure.test)")
                                    (@evaluate (str "(clojure.test/test-vars [#'"
                                                    (:text/contents current-var) "])")))}
     :disconnect {:command #(-> @state :repl/evaluator eval/close!)}
     :doc-for-var {:command #(doc-for-var state)}
     :load-file {:command #(p/let [{:keys [file/filename]} (@eql [:file/filename])
                                   res (@evaluate (str "(load-file " (pr-str filename) ")")
                                         {:namespace nil})]
                             (when (contains? res :result)
                               (@evaluate (str "(println \"Loaded file\"" (pr-str filename) ")")
                                 {:namespace nil})))}
     :clear-console {:command #(when-let [console (get-console)]
                                 (tango-console/clear console))}
     :go-to-var-definition {:command #(goto-definition state)}}))

(defn- maybe-connect-shadow! [host nrepl-evaluator on-output]
  (p/let [result (eval/evaluate nrepl-evaluator
                                "(require 'shadow.cljs.devtools.cli)"
                                {:plain false})]
    (if (:error result)
      nrepl-evaluator
      (orbit-shadow/connect! host nrepl-evaluator on-output))))

(defn- evaluate-feature
  ([state code] (evaluate-feature state code {}))
  ([state code params]
   (p/let [eql (-> @state :editor/features :eql)
           editor-info (eql [{:editor/contents [:file/filename
                                                :text/range
                                                :repl/kind
                                                :repl/namespace]}])
           {:keys [:text/range :repl/namespace :repl/kind]} (:editor/contents editor-info)
           namespace (:namespace params namespace)
           col (:col params (-> range first second))
           row (:row params (ffirst range))
           eval-params (cond-> (-> params
                                   (assoc-in [:options :kind] (keyword (name kind) "eval"))
                                   (dissoc :namespace :filename :row :col))
                         namespace (assoc :namespace namespace)
                         row (assoc :row row)
                         col (assoc :col col))]
     (eval/evaluate (:repl/evaluator @state) code eval-params))))

(defn- update-watch! [state watch]
  (p/let [get-editor-data (-> @state :editor/callbacks :editor-data)
          update-watch (-> @state :editor/callbacks :update-watch)
          editor-data (get-editor-data)]
    (when (= (:filename editor-data) (:file watch))
      (update-watch state watch))))

(defn- to-id [old-id]
  (-> old-id
      (str/replace #"#" "_HASH_")
      (str/replace #"/" "_SLASH_")
      (str/replace #"\\" "_BSLASH_")
      (str/replace #":" "_TWODOTS_")
      (str/replace #"\." "_DOT_")))

(defn- render-watches [state file res]
  (let [watches (get res "watches")]
    (swap! state assoc-in [:repl/watch.info :file] file)
    ((:clear-watches (:editor/features @state)))
    (doseq [watch watches]
      (update-watch! state {:id (to-id (get watch "id"))
                            :text (get watch "id")
                            :icon "icon-eye"
                            :file (get watch "file")
                            :row (get watch "line")}))
    watches))

(defn- display-watches [state file]
  (p/let [evaluator (treat/eql state :repl/evaluator)
          res (eval/evaluate evaluator
                             {:file file}
                             {:plain true :options {:op "get_watches"}})]
    (render-watches state file res)))

(defn remove-watches [state {:keys [file ids]}]
  (let [evaluator (:repl/evaluator @state)]
    (if file
      (p/let [res (eval/evaluate evaluator
                                 {:file file}
                                 {:plain true :options {:op "get_watches"}})]
        (remove-watches state {:ids (->> (get res "watches") (map #(get % "id")) (concat ids))}))
      (doseq [id ids]
        (eval/evaluate evaluator {:watch_id id} {:options {:op "unwatch"}})
        (update-watch! state {:id (to-id id)})))))

(defn- update-watches [state file row delta]
  (p/let [evaluator (treat/eql state :repl/evaluator)
          res (eval/evaluate evaluator
                             {:file file :line row :delta delta}
                             {:plain true :options {:op "update_watch_lines"}})]
    (render-watches state file res)))

(defn- clear-watches! [state]
  (let [con ((:get-console (:editor/callbacks @state)))]
    (set! (.. con (querySelector ".watches") -innerHTML) "")))

(defn- features-for [lang state]
  (let [eql (fn [seed query]
              (let [eql (-> @state :editor/features :eql)]
                (eql seed query)))
        on-start-eval (-> @state :editor/callbacks :on-start-eval)
        on-eval (-> @state :editor/callbacks :on-eval)
        result-for-rendering (case lang
                               :ruby ui-ruby/for-result
                               edn-ui/for-result)]
    {
     ; :autocomplete (autocomplete/generate state)
     :find-definition #(find-definition state)
     :pathom/add-resolver (partial pathom/add-resolver state)
     ;; FIXME - do I need this?
     :pathom/add-pathom-resolvers (partial pathom/add-pathom-resolvers state)
     :pathom/remove-resolvers (partial pathom/remove-resolvers state)
     :pathom/compose-resolver (partial pathom/compose-resolver state)
     :evaluate (partial evaluate-feature state)
     :evaluate-and-render (partial eval-text-in-editor-context
                                   state
                                   on-start-eval
                                   on-eval)
     :start-eval (fn [parameters]
                   (assert (:id parameters) "`:id` is a mandatory parameter")
                   (p/let [params (eql parameters [:file/filename :text/range :editor/data :id])]
                     (on-start-eval state params)))
     :did-eval (fn [parameters]
                 (assert (:id parameters) "`:id` is a mandatory parameter")
                 (assert (:repl/result parameters (:repl/error parameters))
                         "Either `:repl/result` or `:repl/error` is required")
                 (p/let [key (if (contains? parameters :repl/result) :repl/result :repl/error)
                         params (eql parameters [:text/range :file/filename :editor/data])
                         result (assoc params key {:result (key parameters) :id (:id parameters)})]
                   (on-eval state result)))
     :is-config? (fn []
                   (let [{:keys [editor/callbacks]} @state
                         config-dir (:config/directory @state)
                         editor-data ((:editor-data callbacks))]
                     (when-let [file (:filename editor-data)]
                       (-> config-dir
                           (path/relative file)
                           (str/starts-with? "..")
                           not))))
     :display-watches #(display-watches state %)
     :clear-watches #(clear-watches! state)
     :update-watches (fn [file row delta] (update-watches state file row delta))
     :render-watches (fn [file res] (render-watches state file res))
     :result-for-renderer result-for-rendering
     :original-result-for-renderer result-for-rendering
     :remove-watches #(remove-watches state %)}))

(defn- prepare-interpreter [state plugin-name]
  (let [{:keys [register-commands notify]} (:editor/callbacks @state)
        commands (:editor/commands @state)
        [evaluator additional-commands] (int/generate-evaluator plugin-name state)]
    (swap! state assoc-in [:editor/features :interpreter/evaluator] evaluator)
    (-> additional-commands
        (.then (fn [additional]
                 (try
                   (register-commands (concat commands additional) state)
                   (catch :default e (js/console.log e)
                     (throw e)))
                 true))
        (p/catch (fn [error]
                   (register-commands commands state)
                   (js/console.error error)
                   (notify {:type :error
                            :title "Error in config file"
                            :message (pr-str error)})
                   false)))
    evaluator))

(defn- diagnostic [state output]
  (let [parse (-> @state :editor/features :result-for-renderer)
        console (-> @state :editor/callbacks :get-console)
        append-error (fn [error]
                       (let [div (doto (js/document.createElement "div")
                                       (.. -classList (add "content")))
                             hiccup (parse {:error error} state)]
                         (.replaceChildren div (ui/dom hiccup))
                         (tango-console/append (console) div ["icon-bug"])))]
    (cond
      (-> output meta :orbit.shadow/error)
      (append-error output)

      (:orbit.shadow/compile-info output)
      (tango-console/update-build-status (console) (:orbit.shadow/compile-info output))

      (:orbit.shadow/clients-changed output)
      (tango-console/update-clients (console) state (:orbit.shadow/clients-changed output))))

  (when-let [output (get output "hit_auto_watch")]
    (let [{:keys [file line]} output
          {:keys [max-traces]} ((-> @state :editor/callbacks :get-config))
          update-traces (-> @state :editor/callbacks :update-traces)
          id (str (:file output) ":" (-> output :line inc))]
      (swap! state update :repl/tracings #(cond-> (conj % {:file file :row line})
                                            (-> % count (> max-traces)) (subvec 1)))
      (update-traces state)
      (update-watch! state {:id (to-id id)
                            :text id
                            :icon "icon-eye"
                            :file (:file output)
                            :row (:line output)})))

  (when-let [output (get output "hit_watch")]
    (let [id (:id output)]
      (update-watch! state {:id (to-id id)
                            :text id
                            :icon "icon-eye"
                            :file (:file output)
                            :row (:line output)}))))


(defn- ensure-dir-exists! [dir]
  (if (fs/existsSync dir)
    true
    (let [parent (path/dirname dir)
          exists? (ensure-dir-exists! parent)]
      (when exists?
        (fs/mkdirSync dir))
      exists?)))

(defn- make-watcher [state config-directory plugin-name]
  (when (ensure-dir-exists! config-directory)
    (fs/watch config-directory (fn [ & _]
                                 (p/then (prepare-interpreter state plugin-name)
                                         #(when %
                                            ((-> @state :editor/callbacks :notify)
                                             {:type :info
                                              :title "Config reloaded"})))))))

(defn- virtual-get-console []
  (if-let [state (state/get-a-state)]
    ((-> @state :editor/callbacks :get-console))
    (tango-console/view [""])))

(defn- wrap-node [parents ^js capture]
  (let [node (.-node capture)
        texts (map (fn [parent]
                     (let [[kind name] (.-children parent)]
                       (str (.-text kind) " " (.-text name))))
                   parents)
        [node-def [node-body possible-body]] (->> node
                                                  .-children
                                                  (split-with #(not (contains? #{"body_statement" "="} (.-type %)))))
        def-text (->> node-def
                      (map #(.-text ^js %))
                      (str/join " "))
        node-children (cond
                        (nil? node-body) ["nil"]

                        (= "body_statement" (.-type node-body))
                        (->> node-body .-children (mapv #(.-text %)))

                        :single-line-method
                        [(.-text possible-body)])
        parent-name (str/replace (str/join "::" texts) #"(class|module) " "")
        method-name (str parent-name
                         (if (-> capture .-name (= "method")) "#" ".")
                         (->> node-def rest (filter #(-> % .-type (= "identifier"))) first .-text))
        node-lines (cons (str "NREPL.watch!(binding, " (pr-str method-name) ")") node-children)
        num-parents (count parents)
        original-row (-> node .-startPosition .-row)]
    {:row original-row
     :adapted-row (- original-row num-parents)
     :method method-name
     :text (str (str/join "\n" texts)
                "\n" def-text "\n" (str/join "\n" node-lines) "\nend\n"
                (str/join "\n" (repeat num-parents "end")))}))

(defn- get-all-watches [state]
  (p/let [eql (-> @state :editor/features :eql)
          data (p/-> (eql [:editor/data]) :editor/data)
          ^js parsed (.parse treat/parser (:contents data))
          captures-res (. treat/parent-query captures (.-rootNode parsed))

          res
          (->> captures-res
               (reduce (fn [{:keys [parents] :as acc} ^js capture]
                         (let [start-index (.. capture -node -startIndex)
                               parents (->> parents
                                            (filter #(<= start-index (.-endIndex ^js %)))
                                            vec)
                               acc (assoc acc :parents parents)]
                           (if (-> capture .-name (= "parent"))
                             (update acc :parents conj (.-node capture))
                             (update acc :texts conj (wrap-node parents capture)))))
                       {:parents [] :texts []})
               :texts)]
    (.delete parsed)
    res))

(defn- deserialize [string]
  (if (str/starts-with? string "[:result")
    {:result (-> string
                 (str/replace "[:result, " "")
                 (str/replace #"]$" "")
                 rp/parse-ruby-string)}
    {:error (let [error-str (-> string
                                (str/replace "[:error, " "")
                                (str/replace #"]$" "")
                                js/JSON.parse)
                  [_ stack] (re-find #" stack=(.*)\>" error-str)
                  error-str (str/replace-first error-str #" stack=(.*)\>" ">")
                  parsed (rp/parse-ruby-string error-str)]
              (if (symbol? parsed)
                (assoc (rp/->RubyUnknownVal (str parsed))
                       :stack (into [] (js/JSON.parse stack)))
                (assoc parsed
                       :stack (into [] (js/JSON.parse stack)))))}))

(defn- serialize [code]
  (let [wrapped (str "begin; res = " code "\n[:result, res]; rescue Exception => e\n"
                     "  [:error, e.inspect.sub(/\\>$/, ' stack=' + e.backtrace.inspect+'>')]\nend")]
    wrapped))

(defn- wrap-with-lang [lang]
  (case lang
    :ruby {:serdes {:serialize serialize :deserialize deserialize}}
    :clojure {}
    {:serdes {:serialize identity :deserialize identity}}))

(defn- update-traces [state]
  (let [old-timeout (:repl/tracings-timeout @state)]
    (js/clearTimeout old-timeout)
    (swap! state assoc
           :repl/tracings-timeout
           (js/setTimeout #(tango-console/update-traces! state)
                          100))))

;; FIXME: Move the resolvers to Duck and remove this
(defn- update-resolvers! [state]
  (let [add-pathom-resolvers (-> @state :editor/features :pathom/add-pathom-resolvers)]
    (add-pathom-resolvers [treat/doc-for-var treat/current-var treat/completions])))

(defn- get-last-ex [state]
  (p/let [eql (-> @state :editor/features :eql)
          id (str (gensym "last-ex-"))
          editor-info (eql [{:editor/contents [:file/filename
                                               :text/range
                                               :editor/data
                                               :repl/namespace]}])
          editor-info (-> editor-info
                          :editor/contents
                          (dissoc :com.wsscode.pathom3.connect.runner/attribute-errors)
                          (assoc :id id))
          on-start-eval (-> @state :editor/callbacks :on-start-eval)
          _ (on-start-eval state editor-info)
          evaluator (:repl/evaluator @state)
          res (eval/evaluate evaluator {} {:no-wrap true :options {:op "last_exception"}})
          on-eval (-> @state :editor/callbacks :on-eval)
          final-res (-> res
                        (dissoc :result)
                        (assoc :id id)
                        (assoc :error (:error (deserialize (get-in res [:result "result"])))))]
    (on-eval state (with-meta final-res {:tango/error true}))))

(defn- add-watch-cmd [state]
  (p/let [{:text/keys [contents range]} (treat/eql state :editor/contents)
          original-row (ffirst range)
          {:keys [method]} (treat/method-name contents original-row)
          ^js editor (.. js/atom -workspace getActiveTextEditor)
          ^js buffer (.getBuffer editor)
          ^js line (.lineForRow buffer original-row)
          indent (re-find #"^\s+" line)]
    (.setTextInRange buffer
                     (clj->js [[original-row 0] [original-row ##Inf]])
                     (str indent "NREPL.watch!(binding, '" method "')\n" line))))

(defn- load-file-cmd [state]
  (p/let [watches (get-all-watches state)
          eql (-> @state :editor/features :eql)
          res (eql [:file/filename :repl/evaluator :editor/contents])
          evaluator (:repl/evaluator res)
          file (:file/filename res)]

    (doseq [{:keys [method]} watches]
      (p/then (eval/evaluate evaluator {:watch_id method} {:plain true :options {:op "unwatch"}})
              #(update-watch! state {:id (to-id method)})))
    (eval/evaluate evaluator (-> res :editor/contents :text/contents) {:filename file})))

(def ^:private unused-cmds
  #{:run-test-for-var :run-tests-in-ns :load-file :go-to-var-definition})

(defn- replace-clj-commands [state cmds]
  (let [old-cmds (remove (fn [[k]] (unused-cmds k)) cmds)]
    (concat old-cmds
            {:load-file {:command #(load-file-cmd state)}
             :go-to-var-definition {:command #(goto-definition state)}
             :add-watch-command {:command #(add-watch-cmd state)}
             :get-last-exception {:command #(get-last-ex state)}})))

(defn- norm-command [state k langs]
  (let [is-config? (-> @state :editor/features :is-config?)
        notify (-> @state :editor/callbacks :notify)
        editor-data (-> @state :editor/callbacks :editor-data)
        ; cmd (:command v)
        curr-lang (:language (editor-data))
        curr-command (get langs curr-lang)]
        ; state (state/get-state curr-lang)
        ; command (some-> state deref (get-in [:editor/commands k]))]

    (cond
      (is-config?)
      ((:Config langs))

      curr-command
      (curr-command)

      (state/get-state curr-lang)
      (notify {:type :error
               :title "Command not supported"
               :message (str "Command " (name k)
                             " is not supported for " (name curr-lang)
                             " language.")})

      :not-connected
      (notify {:type :error
               :title "Not connected"
               :message (str "REPL not connnected for " (name curr-lang))}))))

(defn- register-commands! [callbacks original-register-commands cmds state]
  (let [editor-data (:editor-data callbacks)
        lang (:language (editor-data))
        ;; FIXME - THIS
        commands (case lang
                   :ruby (replace-clj-commands state cmds)
                   :clojure cmds)
        all-commands (state/set-commands lang commands)
        normalized (map (fn [[cmd langs]] [cmd {:command #(norm-command state cmd langs)}])
                        all-commands)]
    ;     is-config? (-> @state :editor/features :is-config?)
    ;     norm-commands (->> commands
    ;                        (map (fn [[k v]] [k (assoc v :command #(norm-command
    ;                                                                is-config?
    ;                                                                editor-data
    ;                                                                notify
    ;                                                                k v))])))]
    ; (state/set-commands lang commands)
    ; (swap! state assoc :editor/commands (into {} commands))
    (original-register-commands normalized state)))

(defn- make-connection! [lang host port callbacks {:keys [open-console set-console?]}]
  (p/catch
   (p/let [{:keys [config-directory notify on-disconnect register-commands]} callbacks
           console (atom nil)
           options (-> default-opts
                       (assoc #_#_:on-stderr #(tango-console/stderr @console %)
                              :get-console #(deref console)
                              :update-traces update-traces
                              :update-watch tango-console/update-watch!
                              :repl-options (wrap-with-lang lang)
                              :on-disconnect (fn [state]
                                               (on-disconnect state)
                                               (state/disconnect! lang)))
                       (merge callbacks)
                       (assoc :register-commands #(register-commands! callbacks register-commands %1 %2))
                       prepare-console
                       (dissoc :config-directory))
           state (atom {:editor/callbacks options
                        :config/directory config-directory})
           plugin-name (-> config-directory path/basename keyword)
           watcher (make-watcher state config-directory plugin-name)
           callback (partial callback-fn state watcher)
           nrepl (orbit-nrepl/connect! host port callback (:repl-options options))
           orbit-evaluator (maybe-connect-shadow! host nrepl callback)
           commands (commands-for state)
           features (features-for lang state)]
     ;; To not "await" for the promise
     [(notify {:type :info :title "nREPL Connected"})]
     (swap! state merge {:repl/evaluator orbit-evaluator
                         :editor/features features
                         :editor/commands commands
                         :repl/tracings []
                         :text/language lang
                         :repl/info {:host host
                                     :kind :clj
                                     :port port}})

     (when-not (:on-diagnostic callbacks)
       (swap! state assoc-in [:editor/callbacks :on-diagnostic] diagnostic))
     (prepare-patch! state)
     ;; Evaluator stuff

     (when state
       (state/add lang state)
       (prepare-interpreter state plugin-name)
       (pathom/generate-eql-from-state! state)
       (update-resolvers! state)

       (p/then (open-console state)
               (fn [c]
                 (when (and set-console? (not= :clojure lang))
                   (tango-console/prepare! c state))
                 (reset! console c)
                 state)))

     state)
   #(connection-error! % (:notify callbacks))))

(defn connect-config!
  "Connect a config file, with its evaluator"
  [opts]
  (let [{:keys [config-directory]} opts
        options (-> default-opts
                    (merge opts)
                    (assoc :get-console #(virtual-get-console))
                    prepare-console
                    (dissoc :config-directory))
           state (atom {:editor/callbacks options
                        :config/directory config-directory})
           plugin-name (-> config-directory path/basename keyword)
           commands (commands-for state)
           evaluator (prepare-interpreter state plugin-name)
           features (assoc (features-for :Config state)
                           :interpreter/evaluator evaluator)]
       (swap! state merge {:repl/evaluator evaluator
                           :editor/features features
                           :editor/commands commands})
       (state/set-commands :Config commands)
       (prepare-patch! state)
       (pathom/generate-eql-from-state! state)
       state))

(defn connect!
  "Connects to a clojure-like REPL that supports the socket REPL protocol.
Expects host, port, and some configs/callbacks:
* config-directory -> the directory where the config file(s) will be stored
* on-start-eval -> a function that'll be called when an evaluation starts
* on-eval -> a function that'll be called when an evaluation ends
* editor-data -> a function that'll be called when a command needs editor's data.
  Editor's data is a map (or a promise that resolves to a map) with the arguments:
    :contents - the editor's contents.
    :filename - the current file's name. Can be nil if file was not saved yet.
    :range - a vector containing [[start-row start-col] [end-row end-col]], representing
      the current selection
* open-editor -> asks the editor to open an editor. Expects a map with `:filename`,
  `:line` and maybe `:contents`. If there's `:contents` key, it defines a \"virtual
  file\" so it's better to open up an read-only editor
* notify -> when something needs to be notified, this function will be called with a map
  containing :type (one of :info, :warning, or :error), :title and :message
* get-config -> when some function needs the configuration from the editor, this fn
  is called without arguments. Need to return a map with the config options.
* get-rendered-results -> gets all results that are rendered on the editor. This is
  used so that the REPL can 'patch' these results when new data appears (think
  of resolving promises in JS)
* on-patch -> patches the result. Optional, if you send a :get-rendered-results
  callback, one will be generated for you
* prompt -> when some function needs an answer from the editor, it'll call this
  callback passing :title, :message, and :arguments (a vector that is composed by
  :key and :value). The callback needs to return a `Promise` with one of the
  :key from the :arguments, or nil if nothing was selected.
* register-commands -> called with the commands that will be registered when the
  REPL connects and/or when we update the config files with new commands
* on-copy -> a function that receives a string and copies its contents to clipboard
* get-console -> a function that returns the current console for the plug-in, if one exists.
  If this callback is passed, stdout, stderr, and diagnostics will be sent to this console
  by default, so you don't need to pass the next 3 callbacks
* on-stdout -> a function that receives a string when some code prints to stdout
* on-stderr -> a function that receives a string when some code prints to stderr
* on-diagnostic -> a function that receives a Shadow-CLJS output, and renders a diagnostic
  when there are errors or warnings on the code
* on-result -> returns a clojure EDN with the result of code
* on-disconnect -> called with no arguments, will disconnect REPLs. Can be called more
than once
* repl-options -> Will configure nREPL in any way. Check possible options in
`orbit.nrepl.evaluator/connect!`

Returns a promise that will resolve to a map with two repls: :clj/aux will be used
to autocomplete/etc, :clj/repl will be used to evaluate code."
  [host port callbacks console-info]
  (let [lang (:language ((:editor-data callbacks)))]
    (when-not (state/get-state :Config)
      (swap! (state/add :Config (connect-config! callbacks))
             #(-> %
                  (assoc :text/language :Config)
                  (assoc-in [:editor/features :result-for-renderer] edn-ui/for-result))))
    (make-connection! lang host port callbacks console-info)))
