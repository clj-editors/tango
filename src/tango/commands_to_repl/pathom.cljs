(ns tango.commands-to-repl.pathom
  (:require [promesa.core :as p]
            [duck-repled.core :as duck]
            [com.wsscode.pathom3.connect.operation :as pco]

            [clojure.string :as str]
            [clojure.edn :as edn]
            [orbit.evaluation :as eval]
            ["fs" :as fs]))

            ; [com.wsscode.pathom.viz.ws-connector.core :as pvc]
            ; [com.wsscode.pathom.viz.ws-connector.pathom3 :as p.connector]))

(defn- gen-eql [resolvers]
  (duck/gen-eql {:resolvers resolvers}))
                 ; :plugin #(do
                 ;            (prn :A %)
                 ;            (p.connector/connect-env % {::pvc/parser-id ::lazuli}))}))
                 ;
(defn- resolvers-from-state [editor-state]
  (p/let [{:keys [editor/callbacks]} @editor-state
          editor-data ((:editor-data callbacks))
          config ((:get-config callbacks))
          not-found :com.wsscode.pathom3.connect.operation/unknown-value]
    {:editor/data (or editor-data not-found)
     :config/eval-as (:eval-mode config)
     :config/project-paths (vec (:project-paths config))
     :repl/evaluator (:repl/evaluator @editor-state)}))

(def ^:private doc-part
  '(when-let [text (:doc @?state)]
     [:<>
      [:div.space]
      (if (:markdown? @?state)
        [ui/Markdown text]
        [:div.pre.text text])]))

(def ^:private spec-part
  '(when-let [spec (:spec @?state)]
     [:<>
      [:div.space]
      [:div.pre
       (cond-> "Spec:\n"
               (:args spec) (str "  args: " (pr-str (:args spec)) "\n")
               (:ret spec) (str "  ret: " (pr-str (:ret spec)) "\n")
               (:fn spec) (str "  fn: " (pr-str (:fn spec))))]]))

(def ^:private markdown-check
  '(when (:doc @?state)
     [:<>
      [:div.space]
      [:label [:input {:type "checkbox"
                       :checked (:markdown? @?state)
                       :onclick (fn [e]
                                  (swap! ?state update :markdown? not))}]
       " Use markdown"]]))

(def ^:private var-contents
  '(let [get-source (fn [evt]
                      (.preventDefault evt)
                      (.stopPropagation evt)
                      (p/let [info (eql [{:editor/contents [:definition/source]}])]
                        (swap! ?state
                               assoc
                               :var-value
                               (-> info
                                   :editor/contents
                                   :definition/source
                                   :text/contents))))]
     (if (empty? (:arglists @?state))
       [:div.rows
        [:div.space]
        (if (contains? @?state :var-value)
          [ui/Markdown (str "```\n" (:var-value @?state) "\n```")]
          [:div [:a {:href "#"
                     :onclick get-source}
                 "Get contents of var"]])]
       [:div.rows
        [:div.space]
        (if (contains? @?state :var-value)
          [ui/Markdown (str "```\n" (:var-value @?state) "\n```")]
          [:div [:a {:href "#"
                     :onclick get-source}
                 "Get source"]])])))

(defn- improved-doc-for-var [{:var/keys [fqn meta spec]}]
  {:render/doc
   {:html [:div.rows
           '[ui/Cols
             (cond-> [:<>]
               (:macro @?state) (conj [:i "^:macro "])
               (:private @?state) (conj [:i "^:private "]))
             [ui/Title (-> @?state :fqn str)]]
           '(when-let [args (seq (or (:method-params @?state)
                                     (:arglists @?state)))]
              (->> args
                   (map (fn [a] [:li {:key a} (pr-str a)]))
                   (into [:<>])))

           ; '(cond-> [:div.cols]
           ;          (:macro @?state) (conj [:i "macro"])
           ;          (:private @?state) (conj [:i "private"]))
           doc-part
           spec-part
           markdown-check
           var-contents]
    :state (-> meta
               (dissoc :ns)
               (assoc :markdown? false :fqn fqn :spec spec))
    :fns {:get-contents '(fn [_ state value]
                           (assoc state :var-value value))}}})

; (defn- make-regex-prefix [contents range]
;   (let [[_ [row col]] range
;         lines (str/split-lines contents)
;         line (nth lines row)
;         from-end-col (- (count line) col)
;         re (re-pattern (str "([\\d\\w_]*).{" from-end-col "}$"))
;         [_ match] (re-find re line)]
;     (-> match (str/split #"") (->> (str/join ".*")))))

(pco/defresolver complete-code [{:keys [ruby/dissected text/range]}]
  {::pco/output [:completions/code :completions/prefix]
   ::pco/priority 10}
  (prn :dissected dissected)
  (let [start (get-in dissected [:range 0 1])
        end (get-in range [1 1])
        identifier (-> dissected :identifier (subs 0 (- end start)))
        reg (apply str (interpose ".*" identifier))
        lazy-filter (str ".lazy.select { |x| x.inspect =~ /" reg "/ }")
        local-code (str "binding.local_variables" lazy-filter ".map { |m| ['local_var', m.to_s] }.to_a + ")
        method-code (str "__self__.public_methods" lazy-filter ".map { |m| ['pub_method', m.to_s] }.to_a + "
                         "__self__.private_methods" lazy-filter ".map { |m| ['priv_method', m.to_s] }.to_a + "
                         "__self__.protected_methods" lazy-filter ".map { |m| ['prot_method', m.to_s] }.to_a")
        norm-symbol (str "Symbol.all_symbols" lazy-filter ".select { |x| x.inspect !~ /\\\\x/ }")
        code (case (:type dissected)
               "constant" "self.class.constants.map { |i| ['constant', i.to_s] } + ::Object.constants.map { |i| ['constant', i.to_s] }"
               "instance_variable" "instance_variables.map { |i| ['instance_var', i.to_s] }"
               "class_variable" "class_variables.map { |i| ['class_var', i.to_s] }"
               "identifier" (str/replace (str local-code method-code) #"__self__\." "")
               "scope_resolution" (str (:callee dissected) ".constants.map { |i| ['constant', i.to_s] }")
               "call" (if (-> dissected :sep (= "::"))
                        (str (:callee dissected) ".constants.map { |i| ['constant', i.to_s] }")
                        (str/replace method-code #"__self__" (:callee dissected)))
               "simple_symbol" (str norm-symbol ".flat_map { |x| [['symbol', x.to_s + ':'], ['symbol', ':' + x.to_s]] }.to_a"))]
    {:completions/code code
     :completions/prefix identifier}))

(defn- detect-namespace? [eql namespace]
  (p/let [result (eql {:repl/kind :clj
                       :text/contents (str "(require '" namespace ")")}
                      [:repl/result])]
    (-> result :repl/result (:result :not-found) nil?)))

(defn detect-complete [eql]
  (p/let [compliment? (detect-namespace? eql "compliment.core")
          suitable? (detect-namespace? eql "suitable.js-completions")
          suitable? (and suitable? (detect-namespace? eql "suitable.compliment.sources.cljs"))]
    {:compliment? compliment?
     :suitable? (and compliment? suitable?)}))

(defn- gen-clojure-complete [state]
  (let [detection (memoize detect-complete)
        is-config? (-> @state :editor/features :is-config? memoize)]
    (pco/resolver
     'clojure-complete
     {::pco/input [:repl/evaluator :repl/kind :text/language
                   (pco/? :repl/namespace)
                   (pco/? :repl/cljs-env) (pco/? :repl/cljs-eval)
                   :text/contents :text/range]
                   ; :text/current-var]
      ::pco/priority 90
      ::pco/output [:completions/all :completions/prefix]}
     (fn [env input]
       (when (= :clojure (:text/language input))
         (p/let [eql (:q env)
                 current-var (eql (update input :text/range #(-> % (update-in [0 1] dec) (update-in [1 1] dec)))
                                  [:text/current-var])
                 input (cond-> (merge input current-var) (is-config?) (assoc :repl/kind :clj))
                 kind (:repl/kind input)
                 completes (detection eql)
                 complete? (if (= kind :cljs)
                             (:suitable? completes)
                             (:compliment? completes))
                 [[_ editor-col]] (:text/range input)
                 [[_ var-col]] (-> input :text/current-var :text/range)
                 current-var (-> input :text/current-var :text/contents)
                 results (if complete?
                           (p/then (eql input [:completions/external]) :completions/external)
                           (p/then (eql input [{:text/current-var [:completions/var :completions/keyword]}])
                                   (fn [{{:completions/keys [var keyword]} :text/current-var}]
                                     (vec (concat var keyword)))))]
           {:completions/all results
            :completions/prefix (if (-> current-var first #{"[" "(" "{"})
                                  ""
                                  (subs current-var 0 (- editor-col var-col)))}))))))

(pco/defresolver complete-result [{:keys [:completions/code :repl/evaluator
                                          :text/language
                                          :file/filename :text/range]}]
  {::pco/input [:completions/code :repl/evaluator :text/language :text/range
                (pco/? :file/filename)]
   ::pco/priority 90
   ::pco/output [:completions/all]}
  (when (= :ruby language)
    (p/let [params (cond-> {:row (ffirst range) :plain true :no-wrap true}
                     filename (assoc :filename filename))
            result (eval/evaluate evaluator code params)
            result (edn/read-string (pr-str result))]
      {:completions/all (mapv (fn [[kind var]]
                                {:text/contents var
                                 :completion/type (case kind
                                                    "local_var" :local
                                                    "pub_method" :method/public
                                                    "priv_method" :method/private
                                                    "prot_method" :method/protected
                                                    "instance_var" :property
                                                    "class_var" :property
                                                    "constant" :constant
                                                    "symbol" :keyword
                                                    :other)})
                              result)})))

(pco/defresolver ruby-find-definition [{:keys [file/filename text/range ruby/dissected repl/evaluator]}]
  {::pco/output [:definition/filename :definition/row]}
  (p/let [callee (:callee dissected "self")
          callee-class (str callee
                            (if (-> dissected :callee-type #{"scope_resolution" "constant"})
                              ".singleton_class"
                              ".class"))
          assign-is-the-expression? (delay (= (:assign/left-side dissected)
                                              (str (:callee dissected)
                                                   (:sep dissected)
                                                   (:identifier dissected))))
          identifier (str (:identifier dissected)
                          (when (-> dissected :type (= "call") (and @assign-is-the-expression?))
                            "="))
          code (case (:type dissected)
                 "call" (str callee-class ".__lazuli_source_location(:" identifier ")")
                 "identifier" (str callee-class ".__lazuli_source_location(:" identifier ")")
                 "constant" (str "Object.const_source_location(" (:identifier dissected) ".name)")
                 "scope_resolution" (str callee ".const_source_location(" (pr-str (:identifier dissected)) ")")
                 nil)]
    (when code
      (p/let [[file row] (eval/evaluate evaluator
                                        {:code code
                                         :file filename
                                         :line (ffirst range)}
                                        {:plain true :options {:op "eval"}})]
        (when (fs/existsSync file)
          {:definition/filename file
            :definition/row (dec row)})))))

(defn generate-eql-from-state! [state]
  (let [resolver #(resolvers-from-state state)
        resolvers (duck/add-resolver {:inputs []
                                      :outputs [:editor/data
                                                :config/eval-as
                                                :config/project-paths
                                                :repl/evaluator]}
                                     resolver)
        resolvers (duck/add-resolver resolvers
                                     {:inputs [:var/fqn :var/meta (pco/? :var/spec)]
                                      :outputs [:render/doc]}
                                     improved-doc-for-var)
        resolvers (conj resolvers complete-result complete-code
                        ruby-find-definition
                        (gen-clojure-complete state))
        generated-eql (gen-eql resolvers)]
    (swap! state assoc-in [:pathom :original-resolvers] resolvers)
    (swap! state assoc-in [:pathom :global-resolvers] resolvers)
    (swap! state assoc-in [:editor/features :eql] generated-eql)))

(defn- change-resolvers [state-val config fun duck-function]
  (let [old (-> state-val :pathom :global-resolvers)
        new (duck-function old config fun)]
    (-> state-val
        (assoc-in [:pathom :global-resolvers] new)
        (assoc-in [:editor/features :eql] (gen-eql new)))))

(defn add-resolver [state config fun]
  (swap! state change-resolvers config fun duck/add-resolver))

(defn compose-resolver [state config fun]
  (swap! state change-resolvers config fun duck/compose-resolver))

(defn remove-resolvers [state outputs]
  (swap! state
         (fn [repl-state]
           (let [resolvers (-> repl-state :pathom :global-resolvers)
                 have-output? (set outputs)
                 removed (filter #(-> % :config ::pco/output have-output?) resolvers)]
             (-> repl-state
                 (assoc-in [:pathom :global-resolvers] removed)
                 (assoc-in [:editor/features :eql] (gen-eql removed)))))))

(defn add-pathom-resolvers [state resolvers]
  (swap! state
         (fn [repl-state]
           (let [old-resolvers (-> repl-state :pathom :global-resolvers)
                 new (into old-resolvers resolvers)]
             (-> repl-state
                 (assoc-in [:pathom :global-resolvers] new)
                 (assoc-in [:editor/features :eql] (gen-eql new)))))))
