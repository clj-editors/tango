(ns tango.editor-helpers
  (:require ["path" :as path]
            ["fs" :as fs]))

(defn get-possible-port
  [project-paths typed-port]
  (if typed-port
    typed-port
    (let [files [(path/join ".shadow-cljs" "nrepl.port")
                 ".nrepl-port"]
          paths (for [file files
                      path project-paths
                      :let [full-path (path/join path file)]
                      :when (fs/existsSync full-path)]
                  (-> full-path fs/readFileSync str js/parseInt))]
      (first paths))))
