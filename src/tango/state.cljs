(ns tango.state)

(defonce ^:private connections (atom {:states (sorted-map)
                                      :commands {}}))

(defn get-state [lang]
  (let [config-state (get-in @connections [:states :Config])
        config? (some-> config-state deref :editor/features :is-config?)]
    (if (and config-state (config?))
      config-state
      (get-in @connections [:states lang]))))

(defn add [lang state]
  (swap! connections assoc-in [:states lang] state)
  state)

(defn get-a-state []
  (-> @connections :states vals last))

(defn set-commands [lang commands]
  (let [old-commands (-> @connections :commands keys)]
    (doseq [command old-commands]
      (swap! connections update :commands
             #(let [without (update % command dissoc lang)]
                (if (-> without (get command) seq)
                  without
                  (dissoc % command)))))

    (doseq [[cmd {:keys [command]}] commands]
      (swap! connections assoc-in [:commands cmd lang] command)))

  (:commands @connections))

(defn disconnect! [lang]
  (set-commands lang [])
  (swap! connections update :states dissoc lang))

#_(disconnect! :Config)
