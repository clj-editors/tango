(ns tango.ui.interactive
  (:require [promesa.core :as p]
            [tango.ui.elements :as ui]
            [orbit.evaluation :as eval]))

(defn- eval-to-dom! [state result result-state outer]
  (let [evaluator (-> state :editor-state deref :editor/features :interpreter/evaluator)]
    (-> evaluator
        (eval/evaluate (-> result :html pr-str)
                       {:plain true
                        :options {:bindings {'?state result-state
                                             'eql (-> state
                                                      :editor-state
                                                      deref
                                                      :editor/features
                                                      :eql)}}})
        (p/then ui/dom)
        (p/catch #(ui/dom [ui/WithClass ["error"] (.-message ^js %)]))
        (p/then #(do
                   ; (prn :AND? % (.-innerHTML %))
                   (. outer replaceChildren %))))))

(defn interactive [result state]
  (let [outer (ui/dom [:div [:div "..."]])
        result-state (-> result :state atom)]
    (eval-to-dom! state result result-state outer)
    (add-watch result-state ::interactive (fn [_ _ _ _]
                                            (eval-to-dom! state result result-state outer)))
    outer))
