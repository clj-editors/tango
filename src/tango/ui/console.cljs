(ns tango.ui.console
  (:require ["ansi_up" :default Ansi]
            [orbit.shadow.evaluator :as shadow-eval]
            [tango.ui.elements :as ui]
            [clojure.string :as str]))

(defn- create-div [ & class-names]
  (let [div (js/document.createElement "div")
        classes (.-classList div)]
    (doseq [c class-names] (.add classes c))
    div))

(defn view [ & additional-classes]
  (let [div (create-div "tango" "console")
        tab-div (create-div "details")
        full-content-div (create-div "full-content")
        content-div (create-div "content-part")]
    (set! (.-ansi div) (new Ansi))
    (set! (.-tab div) tab-div)
    (set! (.-content div) content-div)
    (.appendChild full-content-div content-div)
    (.appendChild full-content-div (create-div "scroll-anchor"))
    (doseq [c additional-classes] (.. div -classList (add c)))

    (doto div
          (.appendChild tab-div)
          (.appendChild full-content-div))))

(defn- create-content-span [^js console text]
  (let [ansi (.-ansi console)
        html (. ansi ansi_to_html text)
        html (str/replace html #"\n" "<br />")
        content-span (js/document.createElement "span")]
    (set! (.-innerHTML content-span) html)
    content-span))

(defn- create-textual-elements [^js console stream text]
  (let [cell (create-div "cell")
        gutter (create-div "gutter" (name stream))
        content (create-div "content" (name stream))
        content-span (create-content-span console text)
        icon-container (create-div "tango" "icon-container")]
    (. cell appendChild gutter)
    (. gutter appendChild icon-container)
    (. icon-container appendChild (create-div "icon"
                                              (if (= stream :out)
                                                "icon-quote"
                                                "icon-alert")))
    (. cell appendChild content)
    (. content appendChild content-span)
    (. (.-content console) appendChild cell)
    content-span))

(defn- remove-all-pending [^js con]
  (when con
    (mapv (fn [cell] (.removeChild (.-content con) (.-parentNode cell)))
          (. con querySelectorAll ".content.pending"))))

(defn- append-text [^js console stream text]
  (when console
    (let [old-pendings (remove-all-pending console)
          last-gutter-classes (into #{}
                                    (some-> console
                                            (.querySelector ".cell:last-child .gutter")
                                            .-classList))
          content-span (if (contains? last-gutter-classes (name stream))
                         (let [last-content (.querySelector console ".cell:last-child .content")
                               span (create-content-span console text)]
                           (.appendChild last-content span)
                           span)
                         (create-textual-elements console stream text))]
      (doseq [cell old-pendings]
        (.appendChild (.-content console) cell))
      content-span)))

(defn append [console element icons]
  (let [cell (create-div "cell")
        gutter (create-div "gutter")]
    (. cell appendChild gutter)
    (. gutter appendChild (apply create-div "icon" icons))
    (. cell appendChild element)
    (. (.-content console) appendChild cell)))

(defn stdout [console text] (append-text console :out text))
(defn stderr [console text] (append-text console :err text))
(defn clear [^js console]
  (set! (.. console -content -innerHTML ) ""))

(defn- create-shadow-tab! [^js console]
  (when-not (.-shadowTab console)
    (set! (.-shadowTab console)
      (doto (create-div "shadow-cljs")
            (.. -classList (add "clients"))))
    (.appendChild (.-tab console) (.-shadowTab console))
    (.appendChild (.-shadowTab console)
                  (doto (create-div "title") (-> .-innerText (set! "Shadow-CLJS"))))
    (.appendChild (.-shadowTab console) (create-div "build-ids"))
    (.appendChild (.-shadowTab console) (create-div "individual-clients")))

  (.-shadowTab console))

(defn update-clients [^js console connection shadow-clients]
  (let [div (create-shadow-tab! console)
        all-clients (.querySelector div "div.individual-clients")
        select (js/document.createElement "select")
        curr-client-id (-> @connection :repl/evaluator shadow-eval/current-client-id)]
    (set! (.-onchange select)
      (fn [evt]
        (-> @connection :repl/evaluator
            (shadow-eval/change-client-id! (-> evt .-target .-value js/parseInt)))))
    (set! (.-innerHTML all-clients) "")
    (doseq [[client-id {:keys [client-info]}] shadow-clients
            :let [shadow-s (str "Build #" client-id " "
                                (:desc client-info)
                                " (" (-> client-info :build-id name) ")")
                  opt (js/document.createElement "option")]]
      (set! (.-innerText opt) shadow-s)
      (set! (.-value opt) client-id)
      (.appendChild select opt)
      (when (= client-id curr-client-id) (. opt setAttribute "selected" "selected")))

    (.appendChild all-clients (doto (js/document.createElement "label")
                                    (-> .-innerText (set! "Client to eval "))
                                    (.appendChild select)))
    (when-not (.querySelector select "option[selected]")
      (-> @connection :repl/evaluator
          (shadow-eval/change-client-id! (-> shadow-clients keys first))))))

(defn update-build-status [^js console compile-info]
  (let [div (create-shadow-tab! console)
        builds (.querySelector div "div.build-ids")
        build-id (-> compile-info :build-id name)
        build-id-div (if-let [div (.querySelector builds (str "." build-id))]
                       div
                       (.appendChild builds
                                     (create-div build-id)))]
    (set! (.-innerHTML build-id-div)
      (str
       build-id " <span class='tango icon-container'>"
       (if-let [status (:build-status compile-info)]
         (case (:status status)
           :failed "<div class='error'><span class='icon stop'></span></div>"
           :completed "<span class='icon check'></span>"
           "<span class='icon loading'></span></span")
         (case (:type compile-info)
           :build-failure "<div class='error'><span class='icon stop'></span></div>"
           :build-complete (let [success? (->> compile-info
                                               :info
                                               :sources
                                               (filter (fn [s] (-> s :warnings seq)))
                                               empty?)]
                             (if success?
                               "<span class='icon check'></span>"
                               "<div class='error'><span class='icon warn'></span></div>"))
           "<span class='icon loading'></span></span"))))))

(defn all-parsed-results [^js console]
  (into [] (.querySelectorAll console "div.content.result")))

(defn- re-escape [string]
  (str/replace string #"[\|\\\{\}\(\)\[\]\^\$\+\*\?\.\-\/]" "\\$&"))

(defn- summarize-path [state path]
  (let [{:keys [get-config]} (:editor/callbacks @state)
        paths (:project-paths (get-config))
        [matching] (filter #(str/starts-with? path %) paths)
        pattern (when matching
                  (-> matching
                      re-escape
                      (str "[/\\\\]?")
                      re-pattern))
        relative (cond-> path matching (str/replace-first  pattern ""))
        sep (if (str/starts-with? (first paths) "/")
              "/"
              "\\")
        splitted (str/split relative sep)
        [prefix [file]] (split-at (-> splitted count dec) splitted)]
    (str (str/join sep (map #(subs % 0 1) prefix))
         sep file)))

(defn- append-trace! [state console file row]
  (let [open-editor (-> @state :editor/callbacks :open-editor)
        traces (.querySelector console "div.traces")
        crumbs (.-children traces)
        txt (str (summarize-path state file) ":" (inc row))
        a (doto (js/document.createElement "a")
                (.setAttribute "href" "#")
                (-> .-innerText (set! txt))
                (-> .-onclick (set! (fn [^js evt]
                                      (.preventDefault evt)
                                      (open-editor {:file-name file
                                                    :line row})))))
        span (js/document.createElement "span")]
    (when (-> crumbs .-length (> 0))
      (.append span " > "))
    (.appendChild span a)
    (.appendChild traces span)))

(defn update-traces! [state]
  (let [traces (:repl/tracings @state)
        con ((:get-console (:editor/callbacks @state)))
        query (.. con (querySelector "input.search-trace") -value)
        query2 (.. con (querySelector "input.search-trace-not") -value)
        reg (try (re-pattern query) (catch :default _ #""))
        reg2 (try (some-> (not-empty query2) re-pattern) (catch :default _ #""))
        filtered (filter #(re-find reg (:file %)) traces)
        removed (cond->> filtered reg2 (remove #(re-find reg2 (:file %))))
        traces (take-last 20 removed)]
    (set! (.. con (querySelector "div.traces") -innerText) "")
    (doseq [{:keys [file row]} traces]
      (append-trace! state con file row))))

(defn update-watch!
  "Updates a watch expression. It `watch` parameter contains:
* `:id` - some ID that identifies the watch expression
* `:text` - a text that will be displayed in the watch expression
* `:callback` - a callback that will be called when we click the button. If `nil`, no button will
  be displayed, and `icon` will be just a badge with the icon
* `:icon` - an icon for the button
* `:file` - the file being watched
* `:row` - a 0-based row

This code will ALWAYS update the watch with the same ID. If none is found, a new one will be
created. If you only send the `:id`, the watch element for that ID will be removed"
  [state {:keys [id text] :as watch}]
  (let [console ((:get-console (:editor/callbacks @state)))
        open-editor (-> @state :editor/callbacks :open-editor)
        chars (count text)
        text (if (< chars 55)
               text
               (str "..." (subs text (- chars 55))))
        link-elem (delay
                   (doto (js/document.createElement "a")
                         (-> .-innerText (set! text))
                         (.setAttribute "href" "#")
                         (-> .-onclick (set! (fn [evt]
                                               (.preventDefault evt)
                                               (open-editor {:file-name (:file watch)
                                                             :line (:row watch)}))))))
        icon (delay
              (doto (js/document.createElement "button")
                    (.. -classList (add "btn" "icon" (:icon watch)))
                    (-> .-onclick (set! (:callback watch)))))
        parent-elem (. console querySelector (str ".watches *[data-id=" id "]"))]

    (if (-> watch keys (= [:id]))
      (when parent-elem (set! (.-outerHTML parent-elem) ""))
      (let [parent-elem (or parent-elem
                            (doto (js/document.createElement "div")
                                  (.setAttribute "data-id" id)))
            watch-elem (. console querySelector ".watches")]
        (set! (.-innerHTML parent-elem) "")
        (cond
          (:callback watch)
          (. parent-elem appendChild @icon)

          (:icon watch)
          (.. @link-elem -classList (add "inline-block" "status-ignored" "icon" (:icon watch))))
        (. parent-elem appendChild @link-elem)
        (. watch-elem appendChild parent-elem)))))

(defn- clear-file-watches [repl-state]
  (when-let [filename (-> @repl-state :repl/watch.info :file)]
    (let [remove-watches (-> @repl-state :editor/features :remove-watches)]
      (remove-watches {:file filename}))))

(defn prepare! [^js c repl-state]
  (let [contents [:<>
                  [:div {:class "title"} "Trace"]
                  [:div {:class "cols"}
                   [:input {:class ["search-trace" "input-text"] :placeholder "Only"
                            :onchange #(update-traces! repl-state)}]
                   [:input {:class ["search-trace-not" "input-text"] :placeholder "Exclude"
                            :onchange #(update-traces! repl-state)}]]
                  [:div {:class "traces"}]
                  [:div {:class "title"} "Watch Points "
                   [ui/Link {:icon ["icon" "icon-x"] :on-click #(clear-file-watches repl-state)}]]
                  [:div {:class "watches"}]]]
    (.. c (querySelector ".details") (appendChild (ui/dom contents)))
    (clear c)))
