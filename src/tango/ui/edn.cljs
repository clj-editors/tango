(ns tango.ui.edn
  (:require [orbit.serializer :as serializer]
            [promesa.core :as p]
            [clojure.string :as str]
            [tango.ui.interactive :as int]
            [tango.integration.repl-helpers :as helpers]
            [tango.ui.elements :as ui]
            ["source-map-js" :as sm]

            [duck-repled.definition-resolvers :as r]))

(defn- reflection-info-details [result state]
  #_
  (let [index (find-right-seq result :orbit.ui.reflect/info)
        cursor (custom-cursor eval-result [index :val])
        new-state (update-state state cursor {} (:state state))]
    (->> @cursor
         :result
         (map-indexed (fn [parent-idx {:keys [title contents]}]
                        (let [curr-cursor (custom-cursor (:eval-result new-state)
                                                         parent-idx)
                              funs-cursor (custom-cursor curr-cursor
                                                         [(find-right-seq curr-cursor :contents)
                                                          :val])]
                          (when-not (map? contents)
                            (->> contents
                                 (map-indexed (fn [idx _]
                                                [as-html (update-state
                                                          new-state
                                                          (custom-cursor funs-cursor idx)
                                                          {:root? true}
                                                          (create-child-state new-state
                                                                              parent-idx
                                                                              idx))]))
                                 (into [ui/Rows ui/Space [ui/Title title]]))))))
         (into [:<>]))))
;
; (defn MoreInfo [callback]
;   [ui/Link callback ["more-info"]
;     ""])

(defn- Reflect [result state]

    [ui/Cols (pr-str result) " "
     [ui/Link {:on-click (fn [])} "..."]]
  #_
  (let [reflection-info (custom-cursor eval-result :meta)
        without-meta (custom-cursor eval-result :data)
        ratom (:state state)
        reflection-info-open? (:reflection-info-open? @ratom)]

    [ui/Cols [as-html (update-state state
                                    without-meta
                                    {:root? (and (:root? state)
                                                 (not reflection-info-open?))}
                                    (:state state))]
     (when (:root? state)
       [:<>
        ui/Whitespace
        (when (and (:root? state) (not reflection-info-open?) (not (:open? @ratom)))
          [MoreInfo #(swap! ratom assoc :reflection-info-open? true)])
        (when (and (:root? state) reflection-info-open? (not (:open? @ratom)))
          [ui/Children
           [ui/Cols
            [ui/Title "More Info"]
            ui/Whitespace
            [ui/Link #(swap! ratom assoc :reflection-info-open? false) []
             "Hide"]]
           [reflection-info-details (update-state state
                                                  reflection-info
                                                  {}
                                                  (:state state))]])])]))
(declare as-html)

(defn- OpenClose [open? parent-elem-delay open-elem-delay closed-elem-delay]
  (let [is-open? (atom false)
        callback (fn [set-icon]
                   (when closed-elem-delay (.remove @closed-elem-delay))
                   (.remove @open-elem-delay)
                   (if @is-open?
                     (do
                       (when closed-elem-delay (.appendChild @parent-elem-delay @closed-elem-delay))
                       (set-icon "chevron closed"))
                     (do
                       (.appendChild @parent-elem-delay @open-elem-delay)
                       (set-icon "chevron opened")))
                   (swap! is-open? not))
        icon ["chevron" (if open? "opened" "closed")]]
    (when open? (js/setTimeout #(callback identity)))
    [ui/Link {:icon icon :on-click callback}]))

(defn- Copy [contents state]
  (when (:root? state)
    (let [callbacks (-> state :editor-state deref :editor/callbacks)
          copy (:on-copy callbacks)
          notify (:notify callbacks)]
      [ui/Cols ui/Space
       [ui/Link {:icon ["icon" "clipboard" "copy"]
                 :on-click (fn []
                             (copy (pr-str contents))
                             (notify {:type :info :title "Copied to clipboard"}))}]])))

(defn- String [contents state]
  (let [closed-txt (if (-> contents count (> 100))
                     (str (subs (pr-str contents) 0 100) "...")
                     (pr-str contents))
        closed (delay (ui/dom [ui/Cols closed-txt [Copy contents state]]))
        opened (delay (ui/dom [ui/Cols [ui/Icon "quote"] " " [ui/WithClass ["italic"]
                                                              [ui/Text contents]]]))
        col (atom nil)]
    (reset! col (ui/dom [ui/Cols
                         (when (:root? state) [OpenClose (:pre-open? state) col opened closed])
                         @closed]))))

(defn- Sequence [elements start sep end state]
  (let [parent [start
                (->> elements
                     (take 50)
                     (map #(as-html % (assoc state :root? false)))
                     (interpose [ui/Block sep])
                     (into [:<>]))
                (if (-> elements count (> 100))
                  [ui/Block (str sep "..." end)]
                  end)
                [Copy elements state]]
        child (->> elements
                   (map #(as-html % (dissoc state :pre-open?)))
                   (into [ui/Children])
                   ui/dom
                   delay)
        root-elem (atom nil)]
    (reset! root-elem
            (ui/dom
             [ui/Rows
              (into
               [ui/Cols (when (:root? state) [OpenClose (:pre-open? state) root-elem child nil])]
               parent)]))))

(defn- keyval-child [keyvals state]
  (->> keyvals
       (map (fn [[k v]]
              [:<>
               [ui/WithClass ["map-key" "opened"] (as-html k (dissoc state :pre-open?))]
               (as-html v (dissoc state :pre-open?))]))
       (interpose ui/Space)
       (into [ui/Children])
       ui/dom
       delay))

(defn- Map [keyvals separator state]
  (let [parent (->> keyvals
                    (take 50)
                    (map (fn [[k v]]
                           [:<>
                            (as-html k (assoc state :root? false))
                            separator
                            (as-html v (assoc state :root? false))]))
                    (interpose ", ")
                    (into [:<>]))
        child (keyval-child keyvals (dissoc state :pre-open?))
        root-elem (atom nil)]

    (reset! root-elem
            (ui/dom
             [ui/Rows
              [ui/Cols
               (when (:root? state) [OpenClose (:pre-open? state) root-elem child nil])
               "{" parent "}" [Copy keyvals state]]]))))

(defn- Text [result state]
  ; FIXME
  (when (:root? state)
    [ui/Text (:data result) [Copy (:data result) state]]
    [ui/Cols (:data result) [Copy (:data result) state]]))

(defn- Leaf [result state]
  [ui/Cols (pr-str result) [Copy result state]])

(defn- clj-stack [class file row state]
  [ui/WithClass ["stack" "clojure"]
   (demunge (str class))
   " ("
   [ui/Link {:on-click (fn []
                         (p/let [eql (-> state :editor-state deref :editor/features :eql)
                                 res (eql {:ex/filename file :ex/row row, :ex/function-name (str (demunge class))}
                                          [:definition/filename :definition/contents :definition/row])
                                 open-editor (-> state :editor-state deref :editor/callbacks :open-editor)]
                           (when (:definition/filename res)
                             (open-editor {:file-name (:definition/filename res)
                                           :line (:definition/row res)
                                           :contents (-> res :definition/contents :text/contents)}))))}
    (str file ":" row)]
   ")"])

(defn- JavaStacktrace [result state]
  (->> result
       (map (fn [[class method file row]]
              (let [clj-file? (re-find #"\.(clj.?|bb)$" (str file))
                    self-eval? (re-find #"(NO.*FILE|form\-init\d+)" file)
                    col (atom nil)
                    eql (-> state :editor-state deref :editor/features :eql)]
                (when self-eval?
                  (p/then (eql [:editor/filename])
                          (fn [{:keys [editor/filename]}]
                            (.replaceWith @col (ui/dom (clj-stack class filename row state))))))
                (reset! col
                        (ui/dom
                         [ui/Cols  "  at "
                          (if clj-file?
                            (clj-stack class file row state)
                            [ui/WithClass ["stack" "non-clojure"]
                             [ui/Cols
                              (str class)
                              "."
                              (str method)
                              (str " (" file ":" row ")")]])])))))
       (into [ui/Children])))

(defn- demunge-js-name [js-name]
  (if (str/starts-with? js-name "global.")
    js-name
    (-> js-name
        (str/replace #"^Object\." "")
        (str/replace #"\$" ".")
        (str/replace #"(.*)\.(.*)$" "$1/$2")
        demunge)))

(defn- parse-source-map! [state file]
  (p/let [eql (-> state :editor-state deref :editor/features :eql)
          res (eql {:file/filename (str file ".map")}
                   [:file/contents])
          c (-> res :file/contents :text/contents)]
    (when c
      (new (. sm -SourceMapConsumer) c))))

(defn- pos-for [source-map file row col]
  (try
    (.originalPositionFor source-map #js {:line (js/parseInt row)
                                          :column (js/parseInt col)
                                          :source file})
    (catch :default _)))

(defn- temporary-cljs-stack [eql file row col]
  (p/let [r (eql [:repl/evaluator])
          res (r/from-clj file row col (:repl/evaluator r))]
    (assoc res
           :definition/row (dec row)
           :definition/col (dec col))))

(defn- js-stack-link [state file row col]
  (ui/dom
   [ui/Link {:on-click
             (fn []
               (p/let [eql (-> state :editor-state deref :editor/features :eql)
                       res (temporary-cljs-stack eql file row col)
                       #_#_
                       res (eql {:ex/filename file :ex/row row, :ex/function-name "foo/bar"}
                                [:definition/filename :definition/contents :definition/row])
                       open-editor (-> state :editor-state deref :editor/callbacks :open-editor)]
                 (when (:definition/filename res)
                   (open-editor {:file-name (:definition/filename res)
                                 :line (:definition/row res)
                                 :column (:definition/col res)
                                 :contents (-> res :definition/contents :text/contents)}))))}
    (str file ":" row ":" col)]))

(defn- JSStacktrace [result source-map-for state]
  (let [stacktraces (->> result
                         str/split-lines
                         rest
                         (map (fn [row]
                                (drop 1
                                      (or (re-find #"\s*at\s*(.*?)\s*\((.*?):(\d+):(\d+)?" row)
                                          (re-find #"\s*at\s*(.*?)\s*(.*?):(\d+):(\d+)?" row))))))
        source-mapped-links (->> stacktraces
                                 (map (fn [[_ file row col]]
                                        (p/let [sm (source-map-for file)]
                                          (when-let [r (some-> sm (pos-for file row col))]
                                            (js-stack-link state (.-source r) (.-line r) (inc (.-column r))))))))
        js-links (map (fn [[code file row col]] (js-stack-link state file row col))
                      stacktraces)
        full-stacktraces (fn [checked?]
                           (into [ui/Rows]
                                 (map (fn [[code] js cljs]
                                        [ui/Cols "  at " (demunge-js-name code)
                                         " ("
                                         [ui/Promise cljs js (fn [element]
                                                               (if checked? (or element js) js))]
                                         ")"])
                                      stacktraces
                                      js-links
                                      source-mapped-links)))
        stack (atom (ui/dom [full-stacktraces true]))]
    [ui/Children
     [ui/Children [ui/Check {:checked true
                             :on-click (fn [_ checked]
                                         (let [new-stack (ui/dom [full-stacktraces checked])]
                                           (.replaceWith @stack new-stack)
                                           (reset! stack new-stack)))}
                   "Use source map"]
      ui/Line
      @stack]]))

(defn- raw-to-error [result]
  (if-let [[_ cause] (re-find #":cause \"((?:\\\"|\\\\|[^\"])*)\"" (:data result))]
    (if-let [[_ trace] (re-find #":trace \"((?:\\\"|\\\\|[^\"])*)\"" (:data result))]
      {:message (str "Error: " cause) :trace (js/JSON.parse (str "\"" trace "\""))}
      {:message (str "Error: " cause)})
    {:message (str "Error: " (pr-str result))}))

(defn- Error [result state]
  (let [result (cond-> result (instance? serializer/RawData result) raw-to-error)
        cause (cond
                (:message result) (str "Error: " (:message result))
                (:cause result) (str "Error: " (:cause result))
                :else (str (-> result :via first :type)
                           ": "
                           (-> result :via first :message)))]
    [ui/Rows
     [ui/Title cause]
     (when (:root? state)
       [ui/Children
        (when-let [via (:via result)]
          [:<>
           [ui/Title "Via"]
           (as-html via state)])
        (when-let [orig (some-> result :original-object .-form)]
          [:<>
           [ui/Title "Original Error"]
           (as-html orig state)])
        ui/Space
        [ui/Title "Stacktrace"]
        (if (string? (:trace result))
          [JSStacktrace (:trace result) (memoize #(parse-source-map! state %)) state]
          [JavaStacktrace (:trace result) state])])]))

(defn- Tagged [result state]
  (if (-> result .-tag (= 'error))
    [Error (.-form result) state]
    (let [tag (str "#" (.-tag result) " ")
          parent (delay
                  (ui/dom [ui/Cols
                           tag
                           [as-html (.-form result) (assoc state :root? false)]
                           (when (:root? state) [Copy result state])]))
          child (delay
                 (ui/dom
                  [ui/Rows
                   tag
                   [ui/Children [as-html (.-form result) (assoc state :pre-open? true)]]]))
          col (atom nil)]
      (reset! col
              (ui/dom
               [ui/Cols
                (when (:root? state) [OpenClose (:pre-open? state) col child parent])
                @parent])))))

(defn- WrappedError [error state]
  [ui/WithClass ["error"] [as-html (first error) state]])

; ; (defonce invalid (js/Object.))
; (declare loose-edn)
; (defn- inner [string open close]
;   (let [re (re-pattern (str "\\" open "([^\\" close "]*)\\" close))
;         [_ inner rs] (re-matches re string)]
;     (loop [acc []
;            r inner]
;       (prn :I r)
;       (if (seq r)
;         (when-let [[elem r] (loose-edn r)]
;           (prn :elem elem)
;           (prn :rest r)
;           (case (first r)
;             nil (recur (conj acc elem) r)
;             " " (recur (conj acc elem) (subs r 1))
;             nil))
;         [acc rs]))))
; #_
; (let [string (pr-str [[nil]])]
;   (loose-edn string))
;
;
; (let [open "[" close "]"
;       re (re-pattern (str "\\" open "([^\\" close "]*)\\" close))]
;   (prn :RE re)
;   (re-matches #"\[([^\]])\]" (pr-str [[]])))
; (defn- tosymbol [string]
;   (when-let [[_ inner r] (re-matches #"([a-z'A-Z0-9\-.$!?\/><*=\?_:]+)(.*)" string)]
;     [(symbol inner) r]))
;
; (defonce inner-regex "\[([^\]]*)\]")
; (defn truefalsenil [string tfn]
;   (when-let [[_ r] (re-matches (re-pattern (str (pr-str tfn) "(.*)")) string)]
;     (cond
;       (= "" r) [tfn r]
;       (str/starts-with? r " ") [tfn r]
;       :a-symbol (tosymbol string))))
;
; (defn- loose-edn [string]
;   (println :LOOSE string)
;   (case (first string)
;     "[" (inner string "[" "]")
;     "(" (some-> (inner string "(" ")") (update 0 #(apply list %)))
;     "#" (some-> (inner string "#\\{" "}") (update 0 #(into #{} %)))
;     "f" (truefalsenil string false)
;     "t" (truefalsenil string true)
;     "n" (truefalsenil string nil)
;     "\"" (when-let [[_ i r] (re-matches #"\"((?:\\\"|\\\\|[^\"])*)\"(.*)" string)]
;            [i r])
;     (tosymbol string)))

(defn shadow-errors [result state]
  (if-let [errors (:orbit.shadow/errors result)]
    [ui/Rows [ui/Title "Errors compiling code"] ui/Space [ui/Text errors]]
    (let [open-editor (-> state :editor-state deref :editor/callbacks :open-editor)]
      (into [ui/Rows
             [ui/Title (str (->> result
                                 :orbit.shadow/warnings
                                 (mapcat #(or (some-> (:warnings %) distinct)
                                              [%]))
                                 count)
                            " Warnings compiling code")]]
            (for [error (:orbit.shadow/warnings result)
                  warning (or (some-> (:warnings error) distinct)
                              [error])
                  :let [excerpt (:source-excerpt warning)
                        StackLink (if-let [filename (:resource-name error (:file error))]
                                    [ui/Link {:on-click (fn []
                                                          (open-editor {:file-name (:file warning)
                                                                        :line (-> warning :line dec)
                                                                        :column (-> warning :column dec)}))}
                                     (str filename ":" (:line warning))]
                                    (let [eql (-> state :editor-state deref :editor/features :eql)
                                          promise (eql [{:editor/contents [:file/filename :text/range]}])]
                                      [ui/Promise promise
                                       [ui/Text "..."]
                                       #(let [contents (:editor/contents %)
                                              first-row (-> contents :text/range ffirst)
                                              row (+ first-row (:line warning) -1)]
                                          [ui/Link {:on-click (fn []
                                                                (open-editor {:file-name (:file/filename contents)
                                                                              :line (dec row)
                                                                              :column (-> warning :column dec)}))}
                                           (str (:file/filename contents) ":" row)])]))]]
              [ui/Rows
               ui/Line
               (when excerpt
                 [ui/Text (->> excerpt :before (str/join "\n"))
                  "\n"
                  (:line excerpt)
                  (-> warning :column dec (repeat " ") (->> (str/join "") (str "\n")) (str "^^^"))])
               [ui/Text (:msg warning)]
               StackLink])))))


(defn- as-html [result state]
  (let [metadata (meta result)]
    (cond
      (:orbit.ui.reflect/info metadata) (Reflect result state)
      (:orbit.patch/id metadata) [ui/WithClass [(str (:orbit.patch/id metadata))]
                                  [as-html (vary-meta result dissoc :orbit.patch/id) state]]
      ; (:orbit.ui/lazy metadata) (lazy state)
      (:orbit.shadow/error metadata) (shadow-errors result state)
      (:tango/interactive metadata) (int/interactive result state)
      (:tango/wrapped-error metadata) (WrappedError result state)
      ; (:tango/generic-stacktrace metadata) (generic-stacktrace state)
      (instance? serializer/RawData result) (Text result state)
      (serializer/tagged-literal-with-meta? result) (Tagged result state)
      (map? result) (Map result [ui/Block " "] state)
      (string? result) (String result state)
      (vector? result) (Sequence result "[" " " "]" state)
      (set? result) (Sequence result "#{" " " "}" state)
      (coll? result) (Sequence result "(" " " ")" state)
      :else (Leaf result state))))

(defn for-result [result editor-state]
  (let [result (if (:error result)
                 (assoc result :result ^:tango/wrapped-error [(:error result)])
                 result)
        new-eql (helpers/prepare-new-eql editor-state)
        new-editor-state (atom (update @editor-state :editor/features assoc
                                      :eql new-eql
                                      :original-eql (-> @editor-state
                                                        :editor/features
                                                        :eql)))]
    (as-html (:result result)
             {:eval-result result
              :editor-state new-editor-state
              :root? true})))
