(ns tango.ui.elements
  (:require [clojure.string :as str]))

(defn- camelize [kebab]
  (str/replace (name kebab) #"\-(.)" (fn [[_ r]] (str/upper-case r))))

(declare dom)
(defn- from-hiccup [[fst snd & children]]
  (cond
    (fn? fst) (dom (apply fst snd children))

    (= fst :<>)
    (let [frag (js/DocumentFragment.)]
      (.appendChild frag (dom snd))
      (doseq [c children] (.appendChild frag (dom c)))
      frag)

    (keyword? fst)
    (let [[elem & rest] (str/split (name fst) #"\.")
          element (js/document.createElement elem)
          classlist (.-classList element)
          add-class (fn [classes] (doseq [c classes] (.add classlist c)))]
      (add-class rest)
      (cond
        (map? snd)
        (doseq [[k v] snd]
          (case k
            :class (if (string? v) (add-class [v]) (add-class v))
            :style (doseq [[k v] v] (aset (.-style element) (camelize k) v))
            (if (-> k name (str/starts-with? "data-"))
              (aset (.-dataset element)
                    (-> k name (str/replace-first "data-" "") keyword camelize)
                    v)
              (aset element (camelize k) v))))

        (some? snd)
        (.appendChild element (dom snd)))

      (doseq [c children] (.appendChild element (dom c)))
      element)

    :else (throw (ex-info "Not a valid hiccup form" {:form fst}))))

(defn dom [element]
  (cond
    (instance? js/Node element) element
    (string? element) (js/document.createTextNode element)
    (vector? element) (from-hiccup element)
    (nil? element) (js/document.createTextNode "")
    :else (throw (ex-info (str "Can't convert into DOM" (pr-str {:form element}))
                          {:form element}))))

(defn Children [& contents]
  (into [:div.children] contents))

; (defn Icon [icon-name]
;   [:div.tango.icon-container
;    [:span {:class ["icon" (cond-> icon-name (keyword? icon-name) name)]}]])
;
(def Space [:div.space])
(def Line [:hr])

(defn Text [& contents]
  (into [:div.text] contents))

(defn Rows [& contents]
  (into [:div.rows] contents))

(defn Cols [& contents]
  (into [:div.cols] contents))

(defn Title [& contents]
  (into [:div.title] contents))

(defn Block [& contents]
  (into [:div.pre] contents))

(defn WithClass [class & children]
  (into [:div {:class class}] children))

; (defn Link [on-click classes & children]
;   (let [link [:a {:class classes
;                   :href "#"
;                   :on-click (fn [e]
;                               (.preventDefault e)
;                               (.stopPropagation e)
;                               (on-click e))}]]
;     (into link children)))

(defn Icon [icon-name]
  [:div {:class (str "icon-" icon-name)} " "])

(defn Promise [^js promise loading success failure]
  (let [^js element (dom loading)]
    (.then promise #(. element replaceWith (dom [success %])))
    (when failure
      (.catch promise #(. element replaceWith (dom [failure %]))))
    element))

(defn Link
  "Creates a link. It needs a parameter with `:icon` and `:on-click`. The function called on
the link is clicked will receive two parameters - one that is a function that you can call to
change the icon, and the second that is an event (and an implementation detail - if you're
running on DOM API, it'll be an onClick event)."
  ([params] (Link params ""))
  ([{:keys [icon on-click]} text]
   (let [my-element (atom nil)
         change-icon (fn [icon]
                       (let [list (.-classList @my-element)]
                         (set! (.-value list) icon)))
         link [:a {:class icon
                   :href "#"
                   :onclick (fn [e]
                              (.preventDefault e)
                              (.stopPropagation e)
                              (on-click change-icon e))}
               text]]
     (reset! my-element (dom link)))))

(defn Check
  "Creates a check. It needs a parameter with `:icon` and `:on-click`. The function called on
the link is clicked will receive three parameters - one that is a function that you can call to
change the icon, the second a boolean if the checkbox is checked or not, and the third that is
an event (and an implementation detail - if you're running on DOM API, it'll be an onChange event)."
  ([params] (Check params ""))
  ([{:keys [icon on-click checked]} text]
   (let [my-element (atom nil)
         change-icon (fn [icon]
                       (let [list (.-classList @my-element)]
                         (set! (.-value list) icon)))
         link [:label [:input (cond-> {:class icon
                                       :type "checkbox"
                                       :onchange (fn [^js e]
                                                   (.preventDefault e)
                                                   (.stopPropagation e)
                                                   (on-click change-icon (.. e -target -checked) e))}
                                checked (assoc :checked true))]
               " " text]]
     (reset! my-element (dom link)))))

; (def Space [:div.space])


;; FIXME: don't use Atom API here
(defn Markdown
  "Reagent  element to render a Markdown text (defined by `string`). Can be used in interactive
results. Code blocks without any language will be assumed to be Clojure.

Usage - evaluate the following code:
```
^:tango/interactive {:html '[ui/Markdown \"_Hello_, **world!**\"]}
```"
  [string]
  (let [md (.. js/atom -ui -markdown)
        frag (.render md
                      string
                      #js {:renderMode "fragment"
                           :breaks false
                           :useDefaultEmoji true})
        dom (.convertToDOM md frag)]
    (.applySyntaxHighlighting md dom
                              #js {:renderMode "fragment"
                                   :syntaxScopeNameFunc (fn [lang]
                                                          (if (seq lang)
                                                            (str "source." (str/lower-case lang))
                                                            "source.clojure"))})
    [:div.markdown dom]))
