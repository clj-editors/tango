(defproject link.szabo.mauricio/tango "0.6.0"
  :dependencies [[reagent "1.2.0"]
                 [funcool/promesa "6.0.0"]
                 [org.babashka/sci "0.8.40"]
                 [link.szabo.mauricio/duck-repled "2024-02-12.04.17"]]

  :license {:name "MIT"
            :url ""}

  :source-paths ["src"]

  :profiles {:dev {:src-paths ["dev" "test"]
                   :dependencies [[thheller/shadow-cljs "2.24.1"]
                                  [org.clojure/clojurescript "1.11.60"]
                                  [devcards "0.2.5"]
                                  [check/check "0.2.2-SNAPSHOT"]

                                  ;;FiXME
                                  [paprika "0.1.3-SNAPSHOT"]
                                  [org.pinkgorilla/gorilla-renderable-ui "0.1.33"]]}})
